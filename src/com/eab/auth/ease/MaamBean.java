package com.eab.auth.ease;

import java.util.Date;

public class MaamBean {
	public static String accessToken;		// JWT Access Token
	public static String tokenType;			// Token Type e.g. Bearer
	public static int expiresIn;			// Second e.g. 3600 = 60 mins
	public static String refreshToken;		// JWT Refresh Token
	public static String scope;				// Authorization Scope e.g. Scope A
	public static Date updDateTime;			// Get Token Time
	
	public final static int refreshBefore = 300;	// Refreah Access Token Before X second(s)
}
