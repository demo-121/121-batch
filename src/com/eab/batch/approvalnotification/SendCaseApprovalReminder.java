package com.eab.batch.approvalnotification;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.AgentUtil;
import com.eab.common.ApprovalUtil;
import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonObject;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SendCaseApprovalReminder implements Job {
	
	private static final Set<String> ESCAPE_STATUS = new HashSet<String>(Arrays.asList(
		     new String[] {"A","R","E"}
	));
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
		
	public static void startBatch() {
		Log.info("Send Case Approval Reminder Batch Job Start");
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String tempValue = eSysParam.selectSysValue("NO_OF_DAYS_FOR_CASE_APPROVAL_REMINDER");
			int controlValue = (tempValue == null) ? -2 : Integer.parseInt(tempValue);

			String stFilterStr = Long.toString(Function.getLongStartDateFilter(controlValue));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(controlValue));
			
			JSONObject sysParam = Constant.CBUTIL.getDoc("sysParameter");
			int secAssignmentDay = sysParam.has("SECONDARY_PROXY_ASSIGNMENT_DAY") ? sysParam.getInt("SECONDARY_PROXY_ASSIGNMENT_DAY") : 7;
			
			Log.debug("Send Case Approval Reminder Start Range of Filter");
			Log.debug(stFilterStr);
			Log.debug("Send Case Approval Reminder End Range of Filter");
			Log.debug(endFilterStr);
			
			Log.info("Start to get pendingapproval submission View");
	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("submission", "[\"01\",\"pendingapproval\"," + stFilterStr + ",\"0\",\"0\"]", "[\"01\",\"pendingapproval\"," + endFilterStr + ",\"ZZZ\",\"ZZZ\"]");
			
			
			if (viewRows == null) {
				Log.debug("Null Send pending approval case");
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");
			
			if (rows.length() == 0){
				Log.debug("row.length == 0 Send pending approval case");
				return;
			}
			
			JsonObject valueObj;
			String managerCode;

			Set<String> list = new HashSet<String>();
			Set<String> faList = new HashSet<String>();
			
			HashMap<String, String> agentProfile = new HashMap<String, String>();
			Function func = new Function();
			
			int fallInNo;
			int fallInNoFaChannel;
			
			//Update Agents View Index
			Constant.CBUTIL.getRecordsByViewAfterIndex("agents", "[\"01\",\"0\"]", "[\"01\",\"ZZZ\"]");
			for (int i = 0; i < rows.length(); i++){
				try {
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
					
					agentProfile = func.getAgentProfileInfo(func.transformEleToString(valueObj.get("agentId")));
					
					rows = viewRows.getJSONArray("rows");
					fallInNo = 0;
					fallInNoFaChannel =0;
					
//					String assignedManagerCode = ApprovalUtil.getProxySupervisorCode(approverId);
					HashMap<String, JsonObject> rolesObj = ApprovalUtil.getAssignedProfileBySubmissionViewObj(valueObj, secAssignmentDay);
					JsonObject assignedManagerProfileObj = (rolesObj.get("assignedManager") != null) ? rolesObj.get("assignedManager") : new JsonObject();
					
					String assignedManagerCode = func.transformEleToString(assignedManagerProfileObj.get("agentCode"));
					
					if (!ESCAPE_STATUS.contains(func.transformEleToString(valueObj.get("status"))) && func.transformEleToBoolean(valueObj.get("isFACase")) && !assignedManagerCode.equals("")) {
						faList.add(assignedManagerCode);
					} else if (!ESCAPE_STATUS.contains(func.transformEleToString(valueObj.get("status"))) && !assignedManagerCode.equals("")){
						list.add(assignedManagerCode);
					}
									
					
				} catch (Exception exp) {
					Log.error(exp);
				}
			};
	
			List<String> noDupSet = new ArrayList<>();
			List<String> noDupSetforFA = new ArrayList<>();
			noDupSet.clear();
			noDupSet.addAll(list);

			noDupSetforFA.clear();
			noDupSetforFA.addAll(faList);

			handleFallinCase(noDupSet, noDupSetforFA);
			
			Log.info("Send Case Approval Reminder Batch Job End");
		} catch (Exception ex) {
			Log.error(ex);			
		}
	}
	
	private static void handleFallinCase(List list, List faList){
		int bNo=0;
		try {
			if (list.size() > 0) {
				Log.debug("No of Case Approval Reminder Fall in Case: " + list.size());
				Log.debug("Start insert to Batch log table");
				BATCH_LOG bLog = new BATCH_LOG();
				bNo = bLog.selectSeq();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SENDCASEAPPROVALREMINDER", "P", new Date(), list.size());
				
				if (resultPreSub) {
					
					Function func = new Function();
					HashMap<String, String> approverProfile = new HashMap<String, String>();
					
					for (int i = 0; i < list.size(); i++) {
						String managerId = (String) list.get(i);
						Log.debug("No of case with email and sms in case approval reminder: " + managerId);
						approverProfile  = func.getAgentProfileInfo(managerId);
						emailNotification(approverProfile);
						smsNotification(approverProfile);
					}
					
					for (int j = 0; j < faList.size(); j++) {
						String managerId = (String) faList.get(j);
						Log.debug("No of case with email only in case approval FA reminder: " + managerId);
						approverProfile  = func.getAgentProfileInfo(managerId);
						emailNotification(approverProfile);
					} 

				} else {
					throw new Exception("Fail to create record in TABLE Batch_log");
				}
					
				bLog.update(bNo, list.size(), "C", "BATCH");
			}
		}catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, list.size(), "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		
	}
	
	private static boolean emailNotification(HashMap<String, String> approverProfile){
		try {
			HashMap<String, String> validProps = new HashMap<String, String>();
			validProps.put("recipients", approverProfile.get("email"));
			String webDomain = EnvVariable.get("WEB_DOMAIN");
			validProps.put("actionLink", webDomain + "/goApproval");
			

			Email email = NotificationsUtil.getEmailObject(NotificationsType.CASE_APPROVAL_REMINDER, validProps);
						
			NotificationsUtil.sendEmail(email);
			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
	
	private static boolean smsNotification(HashMap<String, String> approverProfile){
		try {
			HashMap<String, String> validSMSProps = new HashMap<String, String>();
			validSMSProps.put("mobileNo", approverProfile.get("mobile"));
			
			SMS sms = NotificationsUtil.getSMSObject(NotificationsType.CASE_APPROVAL_REMINDER, validSMSProps);
						
			NotificationsUtil.sendSms(sms);
			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
}
