package com.eab.batch.approvalnotification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.ApprovalUtil;
import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SendExpiredNotification implements Job {
	private static final Set<String> ESCAPE_STATUS = new HashSet<String>(Arrays.asList(
		     new String[] {"A","R"}
	));
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	public static void startBatch() {
		Log.info("Send Expired Notification  Batch Job Start");
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String tempValue = eSysParam.selectSysValue("NO_OF_DAYS_FOR_EAPPROVAL_SUBMITTED_EXPIRY_DATE");
			int controlValue = (tempValue == null) ? -14 : Integer.parseInt(tempValue);
			int expriyDays = controlValue * -1;
			expriyDays = expriyDays - 1;
			
			JSONObject sysParam = Constant.CBUTIL.getDoc("sysParameter");
			int secAssignmentDay = sysParam.has("SECONDARY_PROXY_ASSIGNMENT_DAY") ? sysParam.getInt("SECONDARY_PROXY_ASSIGNMENT_DAY") : 7;
			
			String stFilterStr = Long.toString(Function.getLongStartDateFilter(controlValue));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(controlValue));
			
			Log.debug("Send Expired Notification  Start Range of Filter");
			Log.debug(stFilterStr);
			Log.debug("Send Expired Notification  End Range of Filter");
			Log.debug(endFilterStr);

	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("submission", "[\"01\",\"expiredNotification\"," + stFilterStr + ",\"0\"]", "[\"01\",\"expiredNotification\"," + endFilterStr + ",\"ZZZ\"]");
			if (viewRows == null) {
				Log.debug("Null Send Expired Notification");
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");
			
			if (rows.length() == 0){
				Log.debug("row.length == 0 Send Expired Notification");
				return;
			}
			
			JsonObject valueObj;
			String submittedDate;
			String agentCode;
			String managerCode;
			String assignedManagerCode = "";
			// String policyId;
			String approvalCaseId;
			String applicationId;
			String fullName;

			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();	
			HashMap<String, String> fallInCase = new HashMap<String, String>();
			HashMap<String, String> agentProfile = new HashMap<String, String>();
			Function func = new Function();
			
			for (int i = 0; i < rows.length(); i++){
				try {
					Log.debug("No. of expired notification: " + rows.length());
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
					submittedDate = func.transformEleToString(valueObj.get("submittedDate"));
					agentCode = func.transformEleToString(valueObj.get("agentId"));
					agentProfile = func.getAgentProfileInfo(func.transformEleToString(valueObj.get("agentId")));
					managerCode = agentProfile.get("managerCode");
					
//					approveManagerCode = ApprovalUtil.getProxySupervisorCode(agentProfile.get("managerCode"));
					HashMap<String, JsonObject> rolesObj = ApprovalUtil.getAssignedProfileBySubmissionViewObj(valueObj, secAssignmentDay);
					assignedManagerCode = (rolesObj.get("assignedManager") != null && rolesObj.get("assignedManager").get("agentCode") != null) ? rolesObj.get("assignedManager").get("agentCode").getAsString() : "";
//					policyId = func.transformEleToString(valueObj.get("policyId"));
					approvalCaseId = func.transformEleToString(valueObj.get("approvalCaseId"));
					applicationId = func.transformEleToString(valueObj.get("applicationId"));
					
					if (ESCAPE_STATUS.contains(func.transformEleToString(valueObj.get("status"))) ) {
						continue;
					}
					
					JsonObject doc = func.transformObject(Constant.CBUTIL.getDoc(applicationId));
					if (doc == null) {
						continue;
					}
//					JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(policyId));
					JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(approvalCaseId));
					
					JsonElement elm = func.getAsPath(doc.getAsJsonObject(), "applicationForm/values/proposer/personalInfo/fullName");
					fullName = elm.getAsString();
					
					fallInCase  = new HashMap<String, String>();
					fallInCase.put("submittedDate", submittedDate);
					fallInCase.put("managerId", managerCode);
					fallInCase.put("assignedManagerCode", assignedManagerCode);
					fallInCase.put("agentId", agentCode);
//					fallInCase.put("policyId", policyId);
					fallInCase.put("approvalCaseId", func.transformEleToString(valueObj.get("approvalCaseId")));
					fallInCase.put("fullName", fullName);
					fallInCase.put("productName", approvalDoc.get("productName").getAsString());
					if (func.transformEleToBoolean(approvalDoc.get("isFACase"))) {
						fallInCase.put("isFACase", "YES");
					}else {
						fallInCase.put("isFACase", "NO");
					}
					
					if (func.transformEleToBoolean(valueObj.get("isShield"))) {
						fallInCase.put("proposalNumber", ApprovalUtil.getCSVString(valueObj.get("subApprovalList").getAsJsonArray()));
					} else {
						fallInCase.put("proposalNumber", func.transformEleToString(valueObj.get("approvalCaseId")));
					}
					
					fallInCase.put("expiredDate", Function.getExpiryDateFrSubmitted( approvalDoc.get("submittedDate").getAsString(), expriyDays, "dd/MM/YYYY"));
					
					
					list.add(fallInCase);
				}catch (Exception exp){
					Log.error(exp);
				}
				
			};
			
			handleFallinCase(list, expriyDays);
			
			Log.info("Send Expired Notification Batch Job End");
		} catch (Exception ex) {
			Log.error(ex);			
		}
	}
	
	private static void handleFallinCase(List list, int expriyDays){
		int bNo=0;
		try {
			if (list.size() > 0) {
				Log.debug("No of Expiry notification Fall in Case: " + list.size());
				Log.debug("Start insert to Batch log table");
				BATCH_LOG bLog = new BATCH_LOG();
				bNo = bLog.selectSeq();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SENDEXPIREDNOTIFICATION", "P", new Date(), list.size());
				
				if (resultPreSub) {
					Function func = new Function();
					HashMap<String, String> agentProfile = new HashMap<String, String>();
					HashMap<String, String> managerProfile = new HashMap<String, String>();
					HashMap<String, String> proxyProfile = new HashMap<String, String>();
					boolean hasProxy = false;
					
					//Update Agents View Index
					Constant.CBUTIL.getRecordsByViewAfterIndex("agents", "[\"01\",\"0\"]", "[\"01\",\"ZZZ\"]");
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, String> fallInCase = (HashMap<String, String>) list.get(i);
						agentProfile = func.getAgentProfileInfo(fallInCase.get("agentId"));
						managerProfile = func.getAgentProfileInfo(fallInCase.get("managerId"));
						
						if (!fallInCase.get("managerId").equalsIgnoreCase(fallInCase.get("assignedManagerCode")) && !fallInCase.get("assignedManagerCode").equals("")) {
							proxyProfile = func.getAgentProfileInfo(fallInCase.get("assignedManagerCode"));
							fallInCase.put("proxyEmail", proxyProfile.get("email"));
							fallInCase.put("proxyMobile", proxyProfile.get("mobile"));
							fallInCase.put("proxyName", proxyProfile.get("name"));
							hasProxy = true;
						}
						
						fallInCase.put("agentEmail", agentProfile.get("email"));
						fallInCase.put("agentMobile", agentProfile.get("mobile"));
						fallInCase.put("agentName", agentProfile.get("name"));
						fallInCase.put("managerEmail", managerProfile.get("email"));
						fallInCase.put("managerMobile", managerProfile.get("mobile"));
						fallInCase.put("managerName", managerProfile.get("name"));
						
						emailNotification(fallInCase, hasProxy);
						if (fallInCase.get("isFACase").equalsIgnoreCase("NO")) {
							smsNotification(fallInCase, hasProxy, expriyDays);
						}
					}

				} else {
					throw new Exception("Fail to create record in TABLE Batch_log");
				}
					
				bLog.update(bNo, list.size(), "C", "BATCH");
			}
		}catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, list.size(), "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		
	}
	
	private static boolean emailNotification(HashMap<String, String> expiryCase, boolean hasProxy){
		try {
			String ccList = "";
			HashMap<String, String> validProps = new HashMap<String, String>();
			validProps.put("recipients", expiryCase.get("agentEmail"));
			validProps.put("adviserName", expiryCase.get("agentName"));
			validProps.put("submissionDate", NotificationsUtil.convertISOStringtoDisplay(expiryCase.get("submittedDate"), "dd/MM/YYYY"));
//			validProps.put("proposalNumber", expiryCase.get("policyId"));
			validProps.put("proposalNumber", expiryCase.get("proposalNumber"));
			validProps.put("proposerName", expiryCase.get("fullName"));
//			validProps.put("caseNumber", expiryCase.get("policyId"));
			validProps.put("caseNumber", expiryCase.get("proposalNumber"));
			validProps.put("productName", expiryCase.get("productName"));
			validProps.put("agentName", expiryCase.get("agentName"));
			if (hasProxy) {
				ccList = expiryCase.get("managerEmail") + "," + expiryCase.get("proxyEmail");
			} else {
				ccList = expiryCase.get("managerEmail");
			}
			validProps.put("cc", ccList);	
			Email email = NotificationsUtil.getEmailObject(NotificationsType.APPROVAL_EXPIRY_NOTIFICATION, validProps);

			NotificationsUtil.sendEmail(email);
			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
	
	private static boolean smsNotification(HashMap<String, String> expiryCase, boolean hasProxy, int expriyDays){
		try {
			HashMap<String, String> validSMSProps = new HashMap<String, String>();
			validSMSProps.put("mobileNo", expiryCase.get("agentMobile"));
			validSMSProps.put("adviserName", expiryCase.get("agentName"));
			validSMSProps.put("managerName", expiryCase.get("managerName"));
			validSMSProps.put("submissionDate", NotificationsUtil.convertISOStringtoDisplay(expiryCase.get("submittedDate"), "dd/MM/YYYY"));
//			validSMSProps.put("proposalNumber", expiryCase.get("policyId"));
			validSMSProps.put("proposalNumber", expiryCase.get("proposalNumber"));
			validSMSProps.put("proposerName", expiryCase.get("fullName"));
			validSMSProps.put("clientName", expiryCase.get("fullName"));
			validSMSProps.put("expiryDate", Function.getExpiryDateFrSubmitted(expiryCase.get("submittedDate"), expriyDays, "dd/MM/YYYY"));
			
			//Send manager SMS
			if (!expiryCase.get("managerMobile").isEmpty()){
				validSMSProps.put("mobileNo", expiryCase.get("managerMobile"));
				SMS managerSms = NotificationsUtil.getSMSObject(NotificationsType.APPROVAL_EXPIRY_NOTIFICATION, validSMSProps);
				NotificationsUtil.sendSms(managerSms);
			}
			
			//Send proxy ManagerSMS
			if (hasProxy) {
				validSMSProps.put("mobileNo", expiryCase.get("proxyMobile"));
				SMS managerSms = NotificationsUtil.getSMSObject(NotificationsType.APPROVAL_EXPIRY_NOTIFICATION, validSMSProps);
				NotificationsUtil.sendSms(managerSms);
			}

			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
}
