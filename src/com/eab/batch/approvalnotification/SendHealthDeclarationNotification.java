package com.eab.batch.approvalnotification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.ApprovalUtil;
import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.object.AgentProfile;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SendHealthDeclarationNotification implements Job {
	private static final Set<String> ESCAPE_STATUS = new HashSet<String>(Arrays.asList(
		     new String[] {"A","R", "E"}
	));
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	public static void startBatch() {
		Log.info("Send Health Declaration Notification  Batch Job Start");
		int bNo = 0;
		int totalLength = 0;
		try {
			
			JSONObject sysParam = Constant.CBUTIL.getDoc("sysParameter");
			
			int notificationDay = sysParam.has("after_pEAppSigned_days") ? sysParam.getInt("after_pEAppSigned_days") : 21;
			int secAssignmentDay = sysParam.has("SECONDARY_PROXY_ASSIGNMENT_DAY") ? sysParam.getInt("SECONDARY_PROXY_ASSIGNMENT_DAY") : 7;
			
			notificationDay = notificationDay * -1;
			String stFilterStr = Long.toString(Function.getLongStartDateFilter(notificationDay));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(notificationDay));
			
			Log.debug("Send Health Declaration Notification Start Range of Filter");
			Log.debug(stFilterStr);
			Log.debug("Send Health Declaration Notification End Range of Filter");
			Log.debug(endFilterStr);

	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("healthDeclarationNotification", "[\"01\"," + stFilterStr + "]", "[\"01\"," + endFilterStr + "]");
			if (viewRows == null) {
				Log.debug("Null Send Health Declaration Notification");
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");
			
			if (rows.length() == 0){
				Log.debug("row.length == 0 Send Health Declaration Notification");
				return;
			}
			totalLength = rows.length();
			BATCH_LOG bLog = new BATCH_LOG();
			bNo = bLog.selectSeq();
			boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SENDHEALTHDECLARATION", "P", new Date(), totalLength);
			
			JsonObject valueObj;
			String appId;
			String approvalId;
			
			Function func = new Function();
			
			boolean allSendSuccess = true;
			if (resultPreSub) {
				for (int i = 0; i < totalLength; i++){
					try {
						Log.debug("No. of Health Declaration Notification: " + totalLength);
						valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
						appId = func.transformEleToString(valueObj.get("appId"));
						
						approvalId = ApprovalUtil.getApprovalIdFrApplicationId(appId);
						
						JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(approvalId));
						boolean result = true;
						if (approvalDoc != null && approvalDoc.has("approvalStatus") && !ESCAPE_STATUS.contains(func.transformEleToString(approvalDoc.get("approvalStatus")))) {
							result = sendNotification(approvalDoc, totalLength, secAssignmentDay);
						} else {
							continue;
						}
						
						if (result == false) {
							allSendSuccess = result;
						}
						
					}catch (Exception exp){
						Log.error(exp);
						allSendSuccess = false;
					}
				};
			}
			
			
			if (allSendSuccess) {
				bLog.update(bNo, rows.length(), "C", "BATCH");
			} else {
				bLog.update(bNo, rows.length(), "F", "BATCH");
			}
		} catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, totalLength, "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
		} finally {
			Log.info("Send Health Declaration Notification Batch Job End");
		}
	}
	
	private static boolean sendNotification(JsonObject approvalDoc, int totalRecords, int secAssignmentDay){
		try {
			Log.debug("No of Health Declaration notification Fall in Case: " + approvalDoc.toString());
			Log.debug("Start insert to Batch log table");
			String agentCode = (approvalDoc.has("agentCode")) ? approvalDoc.get("agentCode").getAsString() : "";
			Function func = new Function();
			
			AgentProfile agentProfile, managerProfile, proxyProfile = null;
			
			boolean hasProxy = false;
			
			//Update Agents View Index
			Constant.CBUTIL.getRecordsByViewAfterIndex("agents", "[\"01\",\"0\"]", "[\"01\",\"ZZZ\"]");

			agentProfile = func.getAgentProfile(agentCode);
			managerProfile = func.getAgentProfile(agentProfile.getManagerCode());
			
			String approvalStatus = approvalDoc.has("approvalStatus") ? approvalDoc.get("approvalStatus").getAsString() : "";
			approvalDoc.addProperty("status", approvalStatus);
			HashMap<String, JsonObject> rolesObj = ApprovalUtil.getAssignedProfileBySubmissionViewObj(approvalDoc, secAssignmentDay);
			
			
			
			String assignedManagerCode = (rolesObj.get("assignedManager") != null && rolesObj.get("assignedManager").get("agentCode") != null) ? rolesObj.get("assignedManager").get("agentCode").getAsString() : "";
			
			if (!agentProfile.getManagerCode().equalsIgnoreCase(assignedManagerCode) && !assignedManagerCode.equals("")) {
				proxyProfile = func.getAgentProfile(assignedManagerCode);
				hasProxy = true;
			}
			
			boolean sendEmailSuccess = emailNotification(approvalDoc, hasProxy, agentProfile, managerProfile, proxyProfile);
			boolean sendSmsSuccess = true;
			if (approvalDoc.has("isFACase") && approvalDoc.get("isFACase").getAsBoolean() == false) {
				sendSmsSuccess = smsNotification(approvalDoc, hasProxy, agentProfile, managerProfile, proxyProfile);
			}
			return sendEmailSuccess && sendSmsSuccess;
		}catch (Exception ex) {
			Log.error(ex);
			return false;
		}
		
	}
	
	private static boolean emailNotification(JsonObject approvalDoc, boolean hasProxy, AgentProfile agentProfile, AgentProfile managerProfile, AgentProfile proxyProfile){
		try {
			String ccList = "";
			HashMap<String, String> validProps = new HashMap<String, String>();
			validProps.put("recipients", agentProfile.getEmail());
			validProps.put("adviserName", agentProfile.getName());
			
			if (approvalDoc.has("policyId")) {
				validProps.put("proposalNumber", approvalDoc.get("policyId").getAsString());
			} else if (approvalDoc.has("proposalNumber")){
				validProps.put("proposalNumber", approvalDoc.get("proposalNumber").getAsString());
			} else {
				validProps.put("proposalNumber", "");
			}
			
			validProps.put("proposerName", approvalDoc.get("proposerName").getAsString());
			validProps.put("productName", approvalDoc.get("productName").getAsString());

			if (hasProxy && proxyProfile != null) {
				ccList = managerProfile.getEmail() + "," + proxyProfile.getEmail();
			} else {
				ccList = managerProfile.getEmail();
			}
			validProps.put("cc", ccList);	
			Email email = NotificationsUtil.getEmailObject(NotificationsType.HEALTH_DECLARATION_REMINDER, validProps);

			NotificationsUtil.sendEmail(email);
			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
	
	private static boolean smsNotification(JsonObject approvalDoc, boolean hasProxy, AgentProfile agentProfile, AgentProfile managerProfile, AgentProfile proxyProfile){
		try {
			HashMap<String, String> validSMSProps = new HashMap<String, String>();
			validSMSProps.put("mobileNo", agentProfile.getMobile());
			validSMSProps.put("adviserName", agentProfile.getName());
			
			if (approvalDoc.has("policyId")) {
				validSMSProps.put("proposalNumber", approvalDoc.get("policyId").getAsString());
			} else if (approvalDoc.has("proposalNumber")){
				validSMSProps.put("proposalNumber", approvalDoc.get("proposalNumber").getAsString());
			} else {
				validSMSProps.put("proposalNumber", "");
			}
			validSMSProps.put("proposerName", approvalDoc.get("proposerName").getAsString());
			
			//Send agent SMS
			if (!agentProfile.getMobile().isEmpty()){
				validSMSProps.put("mobileNo", agentProfile.getMobile());
				SMS agentSms = NotificationsUtil.getSMSObject(NotificationsType.HEALTH_DECLARATION_REMINDER, validSMSProps);

				NotificationsUtil.sendSms(agentSms);
			}

			return true;
		} catch (Exception e) {
			Log.error(e);
			return false;
		}
	}
}
