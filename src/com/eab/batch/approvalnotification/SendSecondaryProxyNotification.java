package com.eab.batch.approvalnotification;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullWriter;
import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.AgentUtil;
import com.eab.common.ApprovalUtil;
import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.enumeration.NotificationsType;
import com.eab.object.Email;
import com.eab.object.SMS;
import com.eab.utils.NotificationsUtil;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.eab.common.EnvVariable;
import com.eab.common.Function;

public class SendSecondaryProxyNotification implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	public static void startBatch() {
		Log.info("Send Secondary Proxy Notification Batch Job Start");
		
		try {	        
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String tempValue = eSysParam.selectSysValue("NO_OF_DAYS_FOR_SEC_PROXY_REMINDER");
			int controlValue = (tempValue == null) ? -7 : Integer.parseInt(tempValue);
			
			String tempDate = eSysParam.selectSysValue("NO_OF_DAYS_FOR_EAPPROVAL_SUBMITTED_EXPIRY_DATE");
			int expriyDays = (tempDate == null) ? 13 : Integer.parseInt(tempDate) * -1;
			expriyDays = expriyDays - 1;
			
			String stFilterStr = Long.toString(Function.getLongStartDateFilter(controlValue));
			String endFilterStr = Long.toString(Function.getLongEndDateFilter(controlValue));
			
			Log.debug("Send Secondary Proxy Notification Start Range of Filter");
			Log.debug(stFilterStr);
			Log.debug("Send Secondary Proxy Notification End Range of Filter");
			Log.debug(endFilterStr);
			
			Log.info("Start to get Secondary Proxy submission View");

	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("submission", "[\"01\",\"secondaryProxyNotification\"," + stFilterStr + ",\"0\"]", "[\"01\",\"secondaryProxyNotification\"," + endFilterStr +",\"ZZZ\"]");
			
			if (viewRows == null) {
				Log.debug("Null Send secondaryProxyNotification Notification");
				return;
			}
			JSONArray rows = viewRows.getJSONArray("rows");

			if (rows.length() == 0){
				Log.debug("row.length == 0 secondaryProxyNotification Director Notification");
				return;
			}
			
			JsonObject valueObj;
//			String policyId;
			String approvalCaseId;
			String applicationId;
			String fullName ="";
			String submittedDate;
			String lastEditedDate;
			boolean isFACase;

			List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();	
			HashMap<String, String> fallInCase = new HashMap<String, String>();
			HashMap<String, String> agentProfile = new HashMap<String, String>();
			Function func = new Function();
			
			for (int i = 0; i < rows.length(); i++){
				try {
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
//					policyId = func.transformEleToString(valueObj.get("policyId"));
					approvalCaseId = func.transformEleToString(valueObj.get("approvalCaseId"));
					isFACase = func.transformEleToBoolean(valueObj.get("isFACase"));
					applicationId = func.transformEleToString(valueObj.get("applicationId"));
					submittedDate = func.transformEleToString(valueObj.get("submittedDate"));
					lastEditedDate = func.transformEleToString(valueObj.get("lastEditedDate"));
					
					JsonObject doc = func.transformObject(Constant.CBUTIL.getDoc(applicationId));
//					JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(policyId));
					JsonObject approvalDoc = func.transformObject(Constant.CBUTIL.getDoc(approvalCaseId));
					
					if (doc == null || approvalDoc == null) {
//						Log.debug("Couchbase doc not found: " + applicationId + ", " + policyId);
						Log.debug("Couchbase doc not found: " + applicationId + ", " + approvalCaseId);
						continue;
					}
	
					JsonElement elm = func.getAsPath(doc.getAsJsonObject(), "applicationForm/values/proposer/personalInfo/fullName");
					if (elm != null) {
						fullName = elm.getAsString();
					}
					
					fallInCase  = new HashMap<String, String>();
					fallInCase.put("submittedDate", func.transformEleToString(valueObj.get("submittedDate")));
					agentProfile = func.getAgentProfileInfo(func.transformEleToString(valueObj.get("agentId")));
					fallInCase.put("managerId", agentProfile.get("managerCode"));
					fallInCase.put("agentId", func.transformEleToString(valueObj.get("agentId")));
//					fallInCase.put("policyId", func.transformEleToString(valueObj.get("policyId")));
					fallInCase.put("fullName", fullName);
					fallInCase.put("approvalStatus", func.transformEleToString(valueObj.get("status")));
					fallInCase.put("productName", approvalDoc.get("productName").getAsString());
					
					fallInCase.put("caseLockedManagerCodebyStatus", func.transformEleToString(approvalDoc.get("caseLockedManagerCodebyStatus")));
					
					fallInCase.put("expiredDate", Function.getExpiryDateFrSubmitted( approvalDoc.get("submittedDate").getAsString(), expriyDays, "dd/MM/YYYY"));
					if (func.transformEleToBoolean(approvalDoc.get("isFACase"))) {
						fallInCase.put("isFACase", "YES");
					}else {
						fallInCase.put("isFACase", "NO");
					}
					
					if (func.transformEleToBoolean(valueObj.get("isShield"))) {
						fallInCase.put("proposalNumber", ApprovalUtil.getCSVString(valueObj.get("subApprovalList").getAsJsonArray()));
					} else {
						fallInCase.put("proposalNumber", func.transformEleToString(valueObj.get("approvalCaseId")));
					}
					
					list.add(fallInCase);
					
				} catch (Exception exp) {
					Log.error(exp);
				}
			};
			
			handleFallinCase(list, expriyDays, controlValue);
			
			Log.info("Send Secondary Proxy Notification Batch Job End");
		} catch (Exception ex) {
			Log.error(ex);			
		}
	}
	
	private static void handleFallinCase(List list, int expriyDays, int secProxyDays){
		int bNo=0;
		try {
			if (list.size() > 0) {
				Log.debug("No of Secondary Proxy Notification Fall in Case: " + list.size());
				Log.debug("Start insert to Batch log table");
				BATCH_LOG bLog = new BATCH_LOG();
				bNo = bLog.selectSeq();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SENDSECPROXYNOTIFICATION", "P", new Date(), list.size());
				
				if (resultPreSub) {
					
					Function func = new Function();
					JsonObject agentProfile = new JsonObject();
					JsonObject proxy2Profile = new JsonObject();
					JsonObject assignedManagerProfile = new JsonObject();
					
					//Get all agents with setting is Proxing or not
					JsonObject agentsWithProxyAssignment = AgentUtil.geAllAgents("01");
					JsonObject mappingUserIdtoAgentCode = AgentUtil.getRawDataAgentsByUserId("01");
					
					//Update Agents View Index
					Constant.CBUTIL.getRecordsByViewAfterIndex("agents", "[\"01\",\"0\"]", "[\"01\",\"ZZZ\"]");
					for (int i = 0; i < list.size(); i++) {
						HashMap<String, String> noActionCase = (HashMap<String, String>) list.get(i);
						
						HashMap<String, JsonObject> rolesObj = ApprovalUtil.getRelatedAgentsWithRoleAssigned("01", noActionCase.get("agentId"), noActionCase, agentsWithProxyAssignment, mappingUserIdtoAgentCode, secProxyDays);
												
						agentProfile = (rolesObj.get("agent") != null) ? rolesObj.get("agent") : new JsonObject();
						proxy2Profile = (rolesObj.get("proxy2") != null) ? rolesObj.get("proxy2") : new JsonObject();
						assignedManagerProfile = (rolesObj.get("assignedManager") != null) ? rolesObj.get("assignedManager") : new JsonObject();
						
						String proxy2Code = (proxy2Profile.get("agentCode") != null) ? proxy2Profile.get("agentCode").getAsString() : "p2";
						String assignedMangerCode  = (assignedManagerProfile.get("agentCode") != null) ? assignedManagerProfile.get("agentCode").getAsString() : "assignedManager";
					
						if(proxy2Code.equalsIgnoreCase(assignedMangerCode)) {							
							noActionCase.put("agentEmail", func.transformEleToString(agentProfile.get("email")));
							noActionCase.put("agentMobile", func.transformEleToString(agentProfile.get("mobile")));
							noActionCase.put("agentName", func.transformEleToString(agentProfile.get("name")));
							noActionCase.put("assignedManagerEmail", func.transformEleToString(assignedManagerProfile.get("email")));
							noActionCase.put("assignedManagerMobile", func.transformEleToString(assignedManagerProfile.get("mobile")));
							noActionCase.put("assignedManagerName", func.transformEleToString(assignedManagerProfile.get("name")));
							
							
							emailNotification(noActionCase, expriyDays);
							if (noActionCase.get("isFACase").equalsIgnoreCase("NO")) {
								smsNotification(noActionCase, expriyDays);
							}
						}
					}

				} else {
					throw new Exception("Fail to create record in TABLE Batch_log");
				}
					
				bLog.update(bNo, list.size(), "C", "BATCH");
			}
		}catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, list.size(), "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		
	}
	
	private static void emailNotification(HashMap<String, String> noActionCase, int expriyDays){
		try {
			
			String ccList = "";
			HashMap<String, String> validProps = new HashMap<String, String>();
			validProps.put("recipients", noActionCase.get("assignedManagerEmail"));
//			validProps.put("proposalNumber", noActionCase.get("policyId"));
			validProps.put("proposalNumber", noActionCase.get("proposalNumber"));
			validProps.put("productName", noActionCase.get("productName"));
			validProps.put("proposerName", noActionCase.get("fullName"));
			validProps.put("agentName", noActionCase.get("agentName"));
			validProps.put("adviserName", noActionCase.get("agentName"));
			validProps.put("clientName", noActionCase.get("fullName"));
//			validProps.put("caseNumber", noActionCase.get("policyId"));
			validProps.put("caseNumber", noActionCase.get("proposalNumber"));
			String webDomain = EnvVariable.get("WEB_DOMAIN");
			validProps.put("actionLink", webDomain + "/goApproval");
			
			//Convert the date format to email display format			
			validProps.put("expiryDate", Function.getExpiryDateFrSubmitted( noActionCase.get("submittedDate"), expriyDays, "dd/MM/YYYY"));
			validProps.put("submissionDate", NotificationsUtil.convertISOStringtoDisplay(noActionCase.get("submittedDate"), "dd/MM/YYYY"));
			
			
			validProps.put("cc", ccList);	
			Email email = NotificationsUtil.getEmailObject(NotificationsType.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION, validProps);
			
			NotificationsUtil.sendEmail(email);
		} catch (Exception e) {
			Log.error(e);
		}
	}
	
	private static void smsNotification(HashMap<String, String> noActionCase, int expriyDays){
		try {
			
			HashMap<String, String> validSMSProps = new HashMap<String, String>();
			validSMSProps.put("mobileNo", noActionCase.get("assignedManagerMobile"));
			validSMSProps.put("adviserName", noActionCase.get("agentName"));
			validSMSProps.put("productName", noActionCase.get("productName"));
			validSMSProps.put("proposerName", noActionCase.get("fullName"));
			validSMSProps.put("submissionDate", noActionCase.get("submittedDate").substring(0, 10));
//			validSMSProps.put("proposalNumber", noActionCase.get("policyId"));
			validSMSProps.put("proposalNumber", noActionCase.get("proposalNumber"));
			validSMSProps.put("clientName", noActionCase.get("fullName"));
			validSMSProps.put("expiryDate", Function.getExpiryDateFrSubmitted( noActionCase.get("submittedDate"), expriyDays, "dd/MM/YYYY"));
			
			SMS sms = null;
			if (!noActionCase.get("assignedManagerMobile").isEmpty()){
				sms = NotificationsUtil.getSMSObject(NotificationsType.SECONDARY_PROXY_ASSIGNMENT_NOTIFICATION, validSMSProps);
			}
			
			NotificationsUtil.sendSms(sms);

		} catch (Exception e) {
			Log.error(e);
		}
	}
}
