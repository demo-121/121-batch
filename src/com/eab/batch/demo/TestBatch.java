package com.eab.batch.demo;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Log;

public class TestBatch implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		doDemo();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	private void doDemo() {
		Log.info("Test Batch Job");
	}
}
