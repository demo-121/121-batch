package com.eab.batch.payment;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.API_PAYMENT_TRX;

public class PaymentStatusJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		StopWatch stopWatch = new StopWatch();

		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");

		stopWatch = null;
	}

	public void startBatch() {
		Log.info("Get payment transaction status");

		Log.debug("*********************        PaymentStatus   START     ***********************");

		API_PAYMENT_TRX trxPaymentDao = new API_PAYMENT_TRX();

		try {
			// TODO get status for record within 2 hours
			JSONArray resultJson = trxPaymentDao.selectInProgressRecords();
			Log.debug("****trxPaymentDao resultJson=" + resultJson.toString());

			if (resultJson != null) {
				for (int i = 0; i < resultJson.length(); i++) {
					JSONObject transactionJson = resultJson.getJSONObject(i);
	
					if (transactionJson.has("APP_ID")) {
						// update application record for timeout
						String appId = null;
						try {
							appId = transactionJson.getString("APP_ID");
							String transactionRef = transactionJson.getString("TRX_NO");
							String payMethod =  transactionJson.getString("METHOD");
							String responseAsString = getStatusFromServiceProvider(transactionRef, payMethod);
							Log.debug("****getStatusFromServiceProvider for "+ transactionRef+" > responseAsString=" + responseAsString);
						
							// update application record for trxEnquiryStatus
							if (StringUtils.isNotEmpty(appId)) {
								JSONObject json = Constant.CBUTIL.getDoc(appId);
								JSONObject payment = json.has("payment")?json.getJSONObject("payment"):null;
								payment.put("trxEnquiryStatus", "Yes");	
								json.put("payment", payment);
								Constant.CBUTIL.UpdateDoc(appId, json.toString());
								Log.debug("*** set trxEnquiryStatus success:"+  appId);
							} else {
								Log.debug("*** set trxEnquiryStatus failure: "+  transactionRef);
							}
						} catch (Exception ex) {
							Log.debug("**** set trxEnquiryStatus failure: " + appId + " - " + ex.getMessage());
						}
					}
				}
			}
			
			
			//  get status for record over 2 hours and status I
			JSONArray timeoutRecords = trxPaymentDao.selectTimeoutRecords();
			Log.debug("****selectTimeoutRecords resultJson=" + timeoutRecords.toString());

			if (timeoutRecords != null) {
				for (int i = 0; i < timeoutRecords.length(); i++) {
					JSONObject transactionJson = timeoutRecords.getJSONObject(i);
	
					if (transactionJson.has("APP_ID")) {
						// update application record for timeout
						String appId = null;
						try {
							String transactionRef = transactionJson.getString("TRX_NO");
							appId = transactionJson.getString("APP_ID");
							if (StringUtils.isNotEmpty(appId)) {
								JSONObject json = Constant.CBUTIL.getDoc(appId);
								JSONObject payment = json.has("payment")?json.getJSONObject("payment"):null; 
								payment.put("trxStatus", "O");
								json.put("payment", payment);
								Constant.CBUTIL.UpdateDoc(appId, json.toString());
								Log.info("Timeout payment for application:"+  appId);

							} else {
								Log.debug("timeout the payment failure:"+  transactionRef);
							}
						} catch (Exception ex) {
							Log.debug("timeout the payment failure:" + appId + " - " + ex.getMessage());
						}
					}
				}
			}
			boolean updateTimeoutRecordResult = trxPaymentDao.updateStatusForTimeoutRecords(); // update status for all
																								// time-out records
			Log.debug("****updateTimeoutRecordResult=" + updateTimeoutRecordResult);
		} catch (Exception ex) {
			Log.error(ex);
		}
		Log.debug("*********************        PaymentStatus   END     ***********************");
	}

	private String getStatusFromServiceProvider(String transactionRef, String payMethod) {
		String responseAsString = null;

		try {
			HttpGet getRequest = createGetStatusRequestOfServiceProvider(transactionRef, payMethod);
			Response output = HttpUtil.send(getRequest);
			responseAsString = output.getEntity().toString();
			Log.debug("****getStatusFromServiceProvider responseAsString=" + responseAsString);

		} catch (Exception ex) {
			Log.error(ex);
		}

		return responseAsString;
	}

	private HttpGet createGetStatusRequestOfServiceProvider(String transactionRef, String payMethod) throws Exception {
		Log.info("Goto PaymentStatusJob.createGetStatusRequestOfServiceProvider");

		String merchantID = EnvVariable.get("WEB_API_EASYPAY2_MID");
		
		if ("crCard".equals(payMethod)) {
		} else if ("eNets".equals(payMethod)) {
			merchantID = EnvVariable.get("WEB_API_EASYPAY2_ENETS_MID");
		} else if ("dbsCrCardIpp".equals(payMethod)) {
			merchantID = EnvVariable.get("WEB_API_EASYPAY2_IPP_MID");
		}		
		
		String queryString = "?mid=" + merchantID + "&ref=" + transactionRef + "&transtype=sale";

		String paymentStatusHost = EnvVariable.get("WEB_API_EASTPAY2_TRX_QUERY");
		 String requestURL = paymentStatusHost+queryString;

		Log.debug("***requestURL=" + requestURL);

		HttpGet request = new HttpGet(requestURL);

		request.addHeader("Content-Type", "application/json");
		request.addHeader("Accept", "application/json");

		return request;
	}
}
