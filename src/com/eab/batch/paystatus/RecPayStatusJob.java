package com.eab.batch.paystatus;

import java.sql.Connection;
import java.sql.ResultSet;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.EnvVariable;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.GET_RECPAY_STATUS;
import com.eab.database.jdbc.DBAccess;

public class RecPayStatusJob implements Job {
	private final static int MAX_CASE = 10;		//0 for unlimited.
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	public void startBatch() {
		GET_RECPAY_STATUS dao = new GET_RECPAY_STATUS();
		Connection conn = null;
		ResultSet rs = null;
		Response resp = null;
		
		Log.info("*** Get Rec. Payment Status - START");
		try {
			conn = DBAccess.getConnection();
			
			// Getting Policy needs to follow up.
			rs = dao.enquiry(conn);
			
			if (rs != null) {
				int count = 0;
				// Get Status by Request ID one by one.
				while(rs.next()) {
					try {
						String reqID = rs.getString("req_id");
						String lob = rs.getString("lob");
						
						if (reqID != null) {
							///TODO: Make max fail count for Rec. Payment status check.
							
							// Call Get Rec. Payment Status API --> /ease-api/payment-record/{reqID}
							resp = callAPI(reqID, lob);
							int httpStatus = (resp!=null)?resp.getStatus():999;
							Log.info("--> Status["+count+"] request_id:" + reqID + ", http_status:" + httpStatus);
						}
					} catch(Exception e) {
						Log.error(e);
					}
					count++;
					
					//MAX_CASE = 0 for unlimited.
					if (MAX_CASE > 0 && count > MAX_CASE)
						break;
				}
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			rs = null;
			resp = null;
			try {if (conn != null && !conn.isClosed()) conn.close();} catch(Exception e) {}
			conn = null;
			dao = null;
		}
	}
	
	private Response callAPI(String reqID, String lob) {
		String apiURL = null;
		HttpGet request = null;
		URIBuilder builder = null;
		
		try {
			apiURL = EnvVariable.get("INTERNAL_API_DOMAIN") + "/payment-record/" + reqID;
			Log.debug("API URL: " + apiURL);
			builder = new URIBuilder(apiURL);
			
			if (lob != null && lob.length() > 0) {
				builder.setParameter("lob", lob);
			}
			
			request = new HttpGet(builder.build());
			return HttpUtil.send(request);
		} catch(Exception e) {
			Log.error(e);
		} finally {
			builder = null;
		}
		
		return null;
	}
}
