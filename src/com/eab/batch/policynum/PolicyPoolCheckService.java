package com.eab.batch.policynum;

import javax.ws.rs.core.Response;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.POOL_CHECK_TRX;
import com.eab.dao.POOL_NON_SHIELD;
import com.eab.dao.POOL_SHIELD;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class PolicyPoolCheckService {
	
	private static final int MAX_FILL_NUM = 1000;

	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private static final String POOL_REFILL_TRIGGER_STRING = EnvVariable.get("POOL_REFILL_TRIGGER");
	private static final double POOL_REFILL_TRIGGER = Double.valueOf(POOL_REFILL_TRIGGER_STRING);
	
	private static final String POLICY_SERVICE_URL = "/PolicyNumRequest";
	
	public boolean needsToRefill(int size, int count) {
		Log.info("Goto PolicyPoolCheckService.needsToRefill");
		double currentPoolRatio = (double)count/size;
		
		Log.debug("POOL_REFILL_TRIGGER="+POOL_REFILL_TRIGGER +", currentPoolRatio="+currentPoolRatio);
		
		return currentPoolRatio < POOL_REFILL_TRIGGER;
	}
	
	public int getFillUpTimes(int size, int count) {
		Log.info("Goto PolicyPoolCheckService.getFillUpTimes");
		return (size-count)/MAX_FILL_NUM + 1;
	}
	
	public int getLastFillNumber(int size, int count) {
		Log.info("Goto PolicyPoolCheckService.getLastFillNumber");
		return (size-count)%MAX_FILL_NUM;
	}

	public void fillPool(String uuid, int num, boolean isHealth) {
		Log.info("Goto PolicyPoolCheckService.fillPool");
		
		Log.debug("*** num="+num +", isHealth="+isHealth);
		
		JsonObject jsonBody = createJsonObjectOfGettingPolicyNumber(num, isHealth);
		
		try {
			HttpPost postRequest = createPostRequestGettingPolicyNumber(jsonBody);
			Response output = HttpUtil.send(postRequest);
			String responseAsString = output.getEntity().toString();
			
//			String responseAsString = "{\"retrievePolicyNumberResponse\":{\"policyNO\":[\"510-5531986\",\"510-5531795\"],\"exception\":{\"reason\":\"Insufficient Policies in ODS.\"}}}";
			Log.debug("****fillPool=" + responseAsString);
			
			if(output.getStatus() == 200) {
				JsonObject bodyJson = Function.validateJsonFormatForString(responseAsString);
				
				if(bodyJson != null) {
					JsonObject retrievePolicyNumberResponse = bodyJson.get("retrievePolicyNumberResponse").getAsJsonObject();
					JsonArray policyNumArray = retrievePolicyNumberResponse.get("policyNO").getAsJsonArray();
					
					if(isHealth && policyNumArray !=null) {
						POOL_SHIELD shieldPoolDao = new POOL_SHIELD();
						shieldPoolDao.insertRecordsFromArray(policyNumArray);
						
						POOL_CHECK_TRX trxPoolCheckDao = new POOL_CHECK_TRX();
						boolean result = trxPoolCheckDao.updateResponseShieldNumber(uuid, policyNumArray.size());
						Log.debug("*** trxPoolCheckDao.updateResponseShieldNumber="+result);
					}
					
					if(!isHealth && policyNumArray !=null) {
						POOL_NON_SHIELD nonShieldPoolDao = new POOL_NON_SHIELD();
						nonShieldPoolDao.insertRecordsFromArray(policyNumArray);
						
						POOL_CHECK_TRX trxPoolCheckDao = new POOL_CHECK_TRX();
						boolean result = trxPoolCheckDao.updateResponseNonShieldNumber(uuid, policyNumArray.size());
						Log.debug("*** trxPoolCheckDao.updateResponseNonShieldNumber="+result);
					}
				}
			}
			
		}catch (Exception ex) {
			Log.error(ex);
		}
	}

	public HttpPost createPostRequestGettingPolicyNumber(JsonObject jsonBody) throws Exception {
		Log.info("Goto PolicyPoolCheckService.createPostRequestGettingPolicyNumber");

		String getPolicyNumberHost = DOMAIN + POLICY_SERVICE_URL;

		HttpPost request = new HttpPost(getPolicyNumberHost);
		request.addHeader("Content-Type", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

	public JsonObject createJsonObjectOfGettingPolicyNumber(int num, boolean isHealth) {
		Log.info("Goto PolicyPoolCheckService.createJsonObjectOfGettingPolicyNumber");

		JsonObject json = new JsonObject();

		String companyCD = isHealth ? "3" : "5";
		String requestedPolicyNO = String.valueOf(num);

		json.addProperty("companyCD", companyCD);
		json.addProperty("requestedPolicyNO", requestedPolicyNO);

		return json;
	}
	
	public void getPolicyNumber(String uuid, int timesNeededToFillUp, int lastFillNum, boolean isHealth) {
		Log.info("Goto PolicyPoolCheckService.getPolicyNumber");
		
		for(int i = 0; i < timesNeededToFillUp; i++) {
			
			int num = MAX_FILL_NUM;
			if(i == timesNeededToFillUp-1) {
				num= lastFillNum;
			}
			if(num>0) {
				fillPool(uuid,num,isHealth);
			}
		}
		
	}

}
