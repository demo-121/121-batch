package com.eab.batch.presubmission;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.Constant;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.eab.common.Constant;
import com.eab.common.EnvVariable;

public class PreSubmission implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	public void startBatch() {
		Log.debug("Pre Submission");
		int bNo=0;
		int successNumber = 0;
		int failNumber = 0;
		try {
	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			
			String params = "[" + "[\"01\",\"submitdoc\",\"A\",false]" + "," + "[\"01\",\"submitdoc\",\"R\",false]" + "," + "[\"01\",\"submitdoc\",\"E\",false]" + "]";
			
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewParamAfterIndex("submission", params);
			
			if (viewRows == null) {
				return;
			}
			
			JSONArray rows = viewRows.getJSONArray("rows");
			
			int bNoforSubReq;
			if (rows.length() == 0){
				return;
			}
		
			Log.debug("Start insert to Batch log table");
			BATCH_LOG bLog = new BATCH_LOG();
			bNo = bLog.selectSeq();
			boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_PRESUBMISSION", "P", new Date(), rows.length());

			Log.debug("End insert to Batch log table");
			if(resultPreSub) {
				JSONObject valueObj;
				String policyId;
				String applicationId;
				String status;
				
				JSONObject updateDoc;
				JsonParser jsonParser = new JsonParser();
				JsonObject transformedUpdateDoc;
				String resultDoc = "";
				
				
				for (int i = 0; i < rows.length(); i++){
					Log.info("Presubmission index " + i);
					valueObj = rows.getJSONObject(i).getJSONObject("value");
					policyId = valueObj.getString("policyId");
					applicationId = valueObj.getString("applicationId");
					status = valueObj.getString("status");
					
					updateDoc = Constant.CBUTIL.getDoc(policyId);
					
					if (updateDoc != null) {
						try {
							Log.debug("Start insert to Batch_Submission_request table");
							
							BATCH_SUBMISSION_REQUEST bSubReq = new BATCH_SUBMISSION_REQUEST();
							bNoforSubReq = bSubReq.selectSeq();
							if (status.equalsIgnoreCase("A")) {
								status = "Approved";
							} else if (status.equalsIgnoreCase("R")) {
								status = "Reject";
							} else if (status.equalsIgnoreCase("E")) {
								status = "Expired";
							}
							boolean resultSubReq = false;
							if(!bSubReq.hasPrimaryKey(applicationId)) {
								resultSubReq = bSubReq.create(applicationId, status, bNo, "R");
							} else {
								transformedUpdateDoc = (JsonObject)jsonParser.parse(updateDoc.toString());
								transformedUpdateDoc.addProperty("submisssionFlag", true);
								resultDoc = Constant.CBUTIL.UpdateDoc(policyId, transformedUpdateDoc.toString());
							}
							
							Log.debug("End insert to Batch_Submission_request table");
							if (resultSubReq){
								
								transformedUpdateDoc = (JsonObject)jsonParser.parse(updateDoc.toString());
								transformedUpdateDoc.addProperty("submisssionFlag", true);
								resultDoc = Constant.CBUTIL.UpdateDoc(policyId, transformedUpdateDoc.toString());
								successNumber++;
							}else {
								failNumber++;
							}
						} catch (Exception ex) {
							Log.error(ex);
							failNumber++;
						}
					}else {
						Log.debug("Null OBject" + i);
						failNumber++;
					}
					
				};
			}

			bLog.update(bNo, successNumber, "C", "BATCH");
		} catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, successNumber, "F", "BATCH");
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
		}
		Log.debug("End of Pre Submission");
	}
}
