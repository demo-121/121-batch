package com.eab.batch.report;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.ATTACH_LOG;
import com.eab.dao.BATCH_EMAIL_TEMPLATE;
import com.eab.dao.SYSTEM_PARAM;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class CPF {
	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN"); 
	private static final String SRS = "srs"; 
	private static final String CPFISOA = "cpfisoa"; 
	private static final String CPFISSA = "cpfissa"; 
	
	public static boolean generateReport(String batchTime){
		try{
			 
			Calendar today = Calendar.getInstance();
	        today.setTimeZone(TimeZone.getTimeZone(Constant.ZONE_ID));
    		int todayYear = today.get(Calendar.YEAR);
    		int todayMonth = today.get(Calendar.MONTH) + 1;  
    		int todayDay = today.get(Calendar.DAY_OF_MONTH);        		 
    		today.add(Calendar.DATE, -1);
    		int yesterdayYear = today.get(Calendar.YEAR);
    		int yesterdayMonth = today.get(Calendar.MONTH) + 1;  
    		int yesterdayDay = today.get(Calendar.DAY_OF_MONTH);             		 
	    
    		//set query time      		 
			String postFix = "T00:00:00.000Z";
			String start ="";
    		String end ="";
    		if(batchTime.equalsIgnoreCase("AM") ) {      			
    			String min = yesterdayYear+"-"+yesterdayMonth+"-"+yesterdayDay+postFix; 
    			String max = todayYear+"-"+todayMonth+"-"+todayDay+postFix;   
    			start  = Long.toString(CPFReportService.dateStr2Long(min, 16,0,0,0));
				end  = Long.toString(CPFReportService.dateStr2Long(max, 8,59,59, 999000000));
    		} else if(batchTime.equalsIgnoreCase("PM") ){	
    			String min = todayYear+"-"+todayMonth+"-"+todayDay+postFix; 
    			String max = todayYear+"-"+todayMonth+"-"+todayDay+postFix; 
    			start  = Long.toString(CPFReportService.dateStr2Long(min, 9,0,0,0));
        		end  = Long.toString(CPFReportService.dateStr2Long(max, 15,59,59, 999000000));
    		} 
    		
    		CPFReportService reportUtil = new CPFReportService();     		  
    		Log.debug("CPF report searchStartTime:"+start + " searchEndTime:"+end); 
    		 
    		//get approval json within a centain time range and payment method
    		JSONObject approvalCases = Constant.CBUTIL.getDoc("_design/main/_view/approvalDateCases?startkey=[\"01\","+start+"]&endkey=[\"01\","+end+"]&stale=ok");
 
    		String[] ePays = new String[]{SRS, CPFISOA, CPFISSA}; 	
	    	JSONArray apps = new JSONArray();
		    for(int i = 0; i< ePays.length;i++){
		    	String ePay = ePays[i];		    	 
		    	JSONObject onlinePayment = Constant.CBUTIL.getDoc("_design/main/_view/cpfApps?startkey=[\"01\",\""+ePay+"\"]&endkey=[\"01\",\""+ePay+"\"]&stale=ok");
		        if(onlinePayment!=null){
		        	JSONArray rows = onlinePayment.getJSONArray("rows");
		        	for (int j = 0; j < rows.length(); j++) {
		        		apps.put(rows.get(j));
			  	    }
		        }
		    }
		    //get payment tamplate for mapping
		    JSONObject paymentTmpl = Constant.CBUTIL.getDoc("paymentTmpl");   
	        
	        List<String> polNums =new ArrayList<String>();
	        if(approvalCases != null){
        	JSONArray approvalCasesRows = (JSONArray)approvalCases.get("rows"); 
            Object[][] records = new Object[approvalCasesRows.length()][];      
            int recCnt = 0;
            for(int i = 0 ; i < approvalCasesRows.length(); i++){
		        	JSONObject approvalCasesRow = (JSONObject) approvalCasesRows.get(i);
		        	JSONObject value = (JSONObject) approvalCasesRow.get("value");
		        	String appId =  value.has("applicationId") && value.get("applicationId") instanceof String ?(String) value.get("applicationId") : null;
		        	String approveRejectDate = value.has("approveRejectDate") && value.get("approveRejectDate") instanceof String ? value.getString("approveRejectDate"):null;		            	
		            String approvalStatus = value.has("approvalStatus") && value.get("approvalStatus") instanceof String ? value.getString("approvalStatus"):null;
		            JSONObject application = (appId != null) ? reportUtil.getDocValue(apps, "id", appId):null;
		            //extract approved record only 
		            if(approvalStatus.equals("A")  && approveRejectDate != null && !approveRejectDate.equals("") && application != null){
             			JSONObject payment = application.has("payment") && application.get("payment") instanceof JSONObject? (JSONObject)application.get("payment") : null;             			 
         				String keyPrefix =  (payment != null) && payment.has("initPayMethod") && payment.get("initPayMethod") instanceof String ? (String)payment.get("initPayMethod"):"";
            			if(keyPrefix != null && (keyPrefix.equals(SRS)||keyPrefix.equals(CPFISOA)||keyPrefix.equals(CPFISSA))){
            				JSONObject block = payment.has(keyPrefix+"Block") &&  payment.get(keyPrefix+"Block") instanceof JSONObject ? (JSONObject)payment.get(keyPrefix+"Block"): null;	                				
            				if(block != null){
                    		    int col = 0;
                		    	Object[] record = new Object[10];
                    		    record[col++] = application.has("policyNumber") &&  application.get("policyNumber") instanceof String ? (String) application.get("policyNumber") : null;
                    		    Calendar cal = Function.convertISO8601(approveRejectDate);
        						Log.debug("approveRejectDate 1:"+ Function.cal2StrFull(cal)); 
        						Function.changeTimezone(cal, Constant.ZONE_ID); 
        						Log.debug("approveRejectDate 2:"+ Function.cal2StrFull(cal));
        						record[col++] = Function.cal2Str(cal);	
                    		    JSONObject covName = application.has("covName") &&  application.get("covName") instanceof JSONObject ? (JSONObject) application.get("covName") : null;
                    		    String prodName = covName != null && covName instanceof JSONObject && covName.has("en") &&  covName.get("en") instanceof String ? (String) covName.get("en") : null;
                    		    record[col++] =  prodName + " (" + CPFReportService.getPaymentMethodName(paymentTmpl, keyPrefix) +")";
                    		    record[col++] = block.has("idCardNo") && block.get("idCardNo") instanceof String ? block.getString("idCardNo"): "";
                    		    String isHaveCpfAcct = block.has(keyPrefix+"IsHaveCpfAcct") && block.get(keyPrefix+"IsHaveCpfAcct") instanceof String ? (String) block.get(keyPrefix+"IsHaveCpfAcct"):"";	                        		    		
                    		    record[col++] = CPFReportService.yesNoConvert(isHaveCpfAcct, "Yes", "No");
                    		    record[col++] = block.has(keyPrefix+"InvmAcctNo") && block.get(keyPrefix+"InvmAcctNo") instanceof String ? (String) block.get(keyPrefix+"InvmAcctNo"):"";
                    		    String bank = block.has(keyPrefix+"BankNamePicker") && block.get(keyPrefix+"BankNamePicker") instanceof String ? (String) block.get(keyPrefix+"BankNamePicker"):"";
                    		    record[col++] = CPFReportService.getBankName(paymentTmpl, bank, keyPrefix);	        	                    		    
                    		    String acctDeclare = block.has(keyPrefix+"AcctDeclare") && block.get(keyPrefix+"AcctDeclare") instanceof String ? (String) block.get(keyPrefix+"AcctDeclare"):"";
                    		    record[col++] = CPFReportService.yesNoConvert(acctDeclare, "Submitted to AXA", "Will open with Bank");	        	                    		    
                    		    String fundReqDate = block.has(keyPrefix+"FundReqDate") && block.get(keyPrefix+"FundReqDate") instanceof String ? (String) block.get(keyPrefix+"FundReqDate"):"";
                    		    record[col++] = CPFReportService.yesNoConvert(fundReqDate, "Yes", "No");
                    		    Long fundProcessDate =  block.has(keyPrefix+"FundProcessDate") ? block.getLong(keyPrefix+"FundProcessDate") :null ; 
                    		    String str_fundDate= null;
                    		    if(fundProcessDate != null && !fundProcessDate.equals("")){
                    		    	Calendar cal_fundDate = Function.convertISO8601(fundProcessDate);
              		              	Log.debug("fundProcessDate 1:"+ Function.cal2StrFull(cal_fundDate)); 
              		              	Function.changeTimezone(cal_fundDate, Constant.ZONE_ID);    
                                    str_fundDate = Function.cal2Str(cal_fundDate);
              		              	Log.debug("fundProcessDate 2:"+ Function.cal2StrFull(cal_fundDate)); 
                    		    }                    		  
                    		    record[col++] =  str_fundDate !=null && str_fundDate.length()>=10? str_fundDate.substring(0, 10) :null;
                    		    records[recCnt] = record; 
                    		    
                    		    polNums.add((String)record[0]);
                    		    recCnt++;
            				}
            			}
             			 
                	}  	            	
            	}    
	            //sort by policy no asc
	            Collections.sort(polNums, new Comparator<String>() {
	                @Override
	                public int compare(String polNum2, String polNum1)
	                {
	                    return  polNum1.compareTo(polNum1);
	                }
	            });	            
	            
	            int cnt=0;
	            Object[][] datalist = new Object[approvalCasesRows.length()][];
	            for(String polNum :polNums){
	            	for(Object[] record :records){
	            		if(((String)record[0]).equals(polNum)){
	            			datalist[cnt] = record;
	            			cnt++;
	            			break;
	            		}	            		
	            	}	            	
	            }
	            
	            String b64Str = CPFReportProvider.createCPFReport(datalist);	
	            ATTACH_LOG attachLog = new ATTACH_LOG(); 
				String jobName = "JOB_BATCH_SUBMIT_REPORT_CPF_"+batchTime;
	            int seqNo = attachLog.selectSeq(); 
	            String fileName =  "CPF Report.xlsx"; 		 
				 
	            attachLog.create(seqNo, jobName , fileName, Base64.decodeBase64(b64Str), new Date());		
	            if(b64Str != null){
	            	boolean success = sendReport(batchTime, fileName, b64Str);
		            if(success)
		            	return true;
	            }
 
		    }
    	}catch(Exception e){
    		Log.error(e); 
    		return false;
    	}
		return false;
    } 
	
	
	 
	private static boolean sendReport(String batchTime, String fileName, String b64Str){
		try{
			Calendar today = Calendar.getInstance();
			today.setTimeZone(TimeZone.getTimeZone(Constant.ZONE_ID));
			int todayYear = today.get(Calendar.YEAR);
			int todayMonth = today.get(Calendar.MONTH) + 1;  
			int todayDay = today.get(Calendar.DAY_OF_MONTH);
			 
			today.add(Calendar.DATE, -1);
			int yesterdayYear = today.get(Calendar.YEAR);
			int yesterdayMonth = today.get(Calendar.MONTH) + 1;  
			int yesterdayDay = today.get(Calendar.DAY_OF_MONTH);       
			
			String strYesterday = yesterdayYear+"/"+yesterdayMonth+"/"+yesterdayDay;
			String strToday = todayYear+"/"+todayMonth+"/"+todayDay; 
			
	        String startTime = "";
	        String endTime = "";
	        String titleTime = "";
	        if(batchTime.equalsIgnoreCase("AM")){
	        	startTime = strYesterday+" 16:00";
	            endTime = strToday+" 08:59:59";
	            titleTime = strToday+" 09:00";
	        }else if (batchTime.equalsIgnoreCase("PM")){
	        	startTime = strToday+" 09:00";
	            endTime = strToday+" 15:59:59";
	            titleTime = strToday+" 16:00";
	        } 
	        
	        SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String toEmail = eSysParam.selectSysValue("REPORT_CPF_TO");
			String fromEmail = eSysParam.selectSysValue("REPORT_CPF_FROM");
			
			Log.debug("*** toEmail="+toEmail+", fromEmail="+fromEmail);
			
			BATCH_EMAIL_TEMPLATE emailTemplateJson= new BATCH_EMAIL_TEMPLATE();
			
			JSONObject templateJsonObject = emailTemplateJson.selectEmailTemplate("CPF_REPORT_BATCH");
			
			String title = templateJsonObject.has("MAIL_SUBJ")? templateJsonObject.getString("MAIL_SUBJ"):null;
			if(title != null){
				title = title.replace("[titleTime]",titleTime);
			}
			String content = templateJsonObject.has("MAIL_CONTENT")? templateJsonObject.getString("MAIL_CONTENT"):null;
			if(content != null){
				content = content.replace("[startTime]",startTime).replace("[endTime]",endTime); 
			}
			JsonObject jsonBody = new JsonObject(); 		
			
			if(toEmail !=null && fromEmail !=null && title !=null && content !=null) {
				JsonObject fileJson = new JsonObject();
				fileJson.addProperty("fileName", fileName);
				fileJson.addProperty("data", b64Str);

				JsonArray attachments = new JsonArray();
				attachments.add(fileJson);

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content",content);
				jsonBody.add("attachments", attachments);
			}	     
			
			String proxyUrl = DOMAIN + "/email/sendEmail";
			HttpPost request = new HttpPost(proxyUrl);
			request.addHeader("Content-Type", "application/json");  
			
			StringEntity stringEntity = new StringEntity(jsonBody.toString());
			request.setEntity(stringEntity); 
	        HttpUtil.send(request);
		}catch(Exception e){
			Log.error("Exception in CPF.sendReport:"+e);
			return false;
		}
        
        return true;
        
		
	}
	
}
