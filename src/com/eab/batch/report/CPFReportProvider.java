package com.eab.batch.report;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 

import com.eab.common.Constant;
import com.eab.common.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;


public class CPFReportProvider {
    
	private final static String[][] header1 = {{
  		  "CPF Report"
  	}};        
	private final static String[][] header2 = {{
  		  "Email - twice a day - 9am and 4pm"
  	}};                
	private final static String[][] subHeader = {{
  		  "Only for OA and SRS / Blank for CPF-SA",
  		  "",
  		  "",
  		  "" 
  	}};        
	private final static String[][] tableHeader = {{
  		  "Policy Number", 
  		  "Submission Date ", 
  		  "Product Name", 
  		  "CPF Account Number (NRIC)", 
  		  "Existing CPF Investment Account?", 
  		  "CPF Investment Account Number", 
  		  "Bank Name", 
  		  "Status of Account Opening Form", 
  		  "CPF Fund Process on Specific Date", 
  		  "CPF Fund Processing Date"
  	}};
    public static String createCPFReport(Object[][] datalist) {
    	try {
	        XSSFWorkbook workbook = new XSSFWorkbook();
	        XSSFSheet sheet = workbook.createSheet("Report"); 
	         
	        int rowNum = 0; 
	        
	        HorizontalAlignment hAlignCenter = org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER;
	        HorizontalAlignment hAlignLeft = org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT;
	        VerticalAlignment vAlignCenter = org.apache.poi.ss.usermodel.VerticalAlignment.CENTER; 
	        FillPatternType fpPatternSolid  = org.apache.poi.ss.usermodel.FillPatternType.SOLID_FOREGROUND;
	        //font
	        XSSFFont font = workbook.createFont();
	        font.setFontHeightInPoints((short) 10);
	        font.setBold(true);
	        
	        XSSFFont font2 = workbook.createFont();
	        font2.setFontHeightInPoints((short) 10);
	        //style color
	        CellStyle subHeaderStyle = workbook.createCellStyle();
	        XSSFColor color = new XSSFColor(new java.awt.Color(204,255,204));
	        ((XSSFCellStyle) subHeaderStyle).setFillForegroundColor(color); 
	        subHeaderStyle.setFillPattern(fpPatternSolid);
	        subHeaderStyle.setAlignment(hAlignCenter);
	        subHeaderStyle.setFont(font);
	        setDefaultBorder(subHeaderStyle);     
	        
	        CellStyle tableHeaderStyle = workbook.createCellStyle();         
	        color = new XSSFColor(new java.awt.Color(204,236,255));        
	        ((XSSFCellStyle) tableHeaderStyle).setFillForegroundColor(color);
	        tableHeaderStyle.setFillPattern(fpPatternSolid);
	        tableHeaderStyle.setAlignment(hAlignCenter);
	        tableHeaderStyle.setVerticalAlignment(vAlignCenter);
	        tableHeaderStyle.setFont(font);
	        tableHeaderStyle.setWrapText(true);
	        setDefaultBorder(tableHeaderStyle);
	        
	        CellStyle header1Style = workbook.createCellStyle();        
	        header1Style.setFillForegroundColor(HSSFColor.WHITE.index);        
	        header1Style.setFont(font);
	        header1Style.setAlignment(hAlignCenter);
	        
	        CellStyle header2Style = workbook.createCellStyle();
	        header2Style.setFont(font);
	        header2Style.setAlignment(hAlignLeft);    
	        
	        
	        CellStyle dataStyleCenter = workbook.createCellStyle();
	        dataStyleCenter.setAlignment(hAlignCenter);
	        dataStyleCenter.setVerticalAlignment(vAlignCenter);        
	        dataStyleCenter.setFont(font2);
	        dataStyleCenter.setWrapText(true);
	        setDefaultBorder(dataStyleCenter);
	        
	        CellStyle dateStyle = workbook.createCellStyle();
	        setCenter(dateStyle);
	        
	        CellStyle dataStyleLeft = workbook.createCellStyle();
	        dataStyleLeft.setAlignment(hAlignLeft);      
	        dataStyleLeft.setWrapText(true);        
	        dataStyleLeft.setVerticalAlignment(vAlignCenter);     
	        dataStyleLeft.setFont(font2);
	        setDefaultBorder(dataStyleLeft);
	        
	        
	        //set col width
	        setColWidth( sheet ,tableHeader[0].length, null, 5000);
	        setColWidth( sheet ,tableHeader[0].length, new int[]{0,1,2,3,5,9}, 7000);
	        
	        //set text and style
	        setText( sheet , header1, header1Style,rowNum, 0) ;
	        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,0,1));
	        rowNum =rowNum+ header1.length;
	        
	        setText( sheet , header2, header2Style,rowNum, 0) ;
	        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,0,1));
	        rowNum =rowNum+ header2.length;  
	        
	        setText( sheet , subHeader, subHeaderStyle,rowNum, 4) ;
	        sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum,4,7));                 
	        rowNum =rowNum+ subHeader.length;
	        
	        setText( sheet , tableHeader, tableHeaderStyle,rowNum, 0) ;
	        rowNum =rowNum+ tableHeader.length;    
	        setText( sheet , datalist, dataStyleLeft,rowNum, 0) ; 
	        setStyle(sheet,rowNum,0,datalist.length,tableHeader[0].length,null,new int[]{1,9} ,dateStyle );
	        rowNum =rowNum+ datalist.length; 
        
        
        	//convert outputStream to base64 string
        	ByteArrayOutputStream  outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);  
			byte[] img64 = Base64.encodeBase64(outputStream.toByteArray());
            String b64Str = new String(img64);			 
            outputStream.close();             
           
            return   b64Str;               
        
        }catch (Exception e) {
        	Log.error("createCPFReport exception: "+e);  
        }
		return null; 
    }
     
    
    private  static void setText(XSSFSheet sheet ,Object[][] datalist, CellStyle cellStyle,int rowNum, int col){      
        for (Object[] data : datalist) {
        	Row row = sheet.createRow(rowNum);
            int colNum = col;
            if( data != null){
            	 for (Object field : data) {            		 
                     Cell cell = row.createCell(colNum); 
                     if(cellStyle != null)
            			 cell.setCellStyle(cellStyle);
                     if (field instanceof String) {                    	 
                         cell.setCellValue((String) field); 
                     } else if (field instanceof Integer) {                    	 
                         cell.setCellValue((Integer) field);
                     }else if (field instanceof Long) {                    	  
                         cell.setCellValue((Long) field);
                     }else if (field instanceof Date) {                    	  
                         cell.setCellValue((Date) field);
                     }
                     colNum++;
                 }
            }
            rowNum++;
        }
    }
     

    private static boolean findInArray(int array[], int idx){
    	for( int i =0 ; i< array.length; i++){
    		if(array[i] == idx)
    			return true;
    	}
    	return false;
    }
    
    private  static void setColWidth(XSSFSheet sheet ,int colNum, int[] spCols, int width){      
	    for(int i =0; i < colNum ; i++){ 
	    	if(spCols == null)
	    		sheet.setColumnWidth(i, width); 
	    	else if(findInArray(spCols, i) )
	    		sheet.setColumnWidth(i, width); 
	    }
    }
    
    private  static CellStyle setDefaultBorder(CellStyle cellStyle){
    	BorderStyle borderThin = org.apache.poi.ss.usermodel.BorderStyle.THIN;
    	cellStyle.setBorderBottom(borderThin);
        cellStyle.setBorderTop(borderThin);
        cellStyle.setBorderRight(borderThin);
        cellStyle.setBorderLeft(borderThin);
        return cellStyle;
    }
    
    private static CellStyle setCenter(CellStyle recordCenter){    	
	    recordCenter.setFillForegroundColor(HSSFColor.WHITE.index);
	    recordCenter.setFillPattern(org.apache.poi.ss.usermodel.FillPatternType.SOLID_FOREGROUND);
	    recordCenter.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    recordCenter.setWrapText(true);
	    recordCenter.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER); 
	    setDefaultBorder(recordCenter);
	    return recordCenter;
    }   
    
    private static void StyleCells(Row row, int startCol, int[] toStyleCols, CellStyle cellStyle, int endCol) {
		if(null == row){
			return;
		}
		if(null == toStyleCols){
			for(int j=startCol;j<=endCol;j++){
				Cell cell = row.getCell(j);
				if(null != cell){
					cell.setCellStyle(cellStyle);
				}
			}
		}else{
			for(int j : toStyleCols){
				if(j > endCol){
					continue;
				}
				Cell cell = row.getCell(j);
				if(null != cell){
					cell.setCellStyle(cellStyle);
				}
			}
		}
	}
    
    private  static void setStyle(XSSFSheet sheet , int startRow, int startCol, int rows, int cols,int[] toStyleRows, int[] toStyleCols, CellStyle cellStyle){
    	int endRow = startRow + rows - 1;
    	int endCol = startCol + cols - 1;
    	if(null == toStyleRows){
    		for(int i=startRow;i<=endRow;i++){
        		Row row = sheet.getRow(i);
        		StyleCells(row,startCol, toStyleCols, cellStyle, endCol);
        	}
    	}else{
    		for(int i : toStyleRows){
    			if(i > endRow){
    				continue;
    			}
    			Row row = sheet.getRow(i);
    			StyleCells(row,startCol, toStyleCols, cellStyle, endCol);
    		}
    	}
    	
    }
}
