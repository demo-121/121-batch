package com.eab.batch.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constant;
import com.eab.common.Log;

public class CPFReportService { 
 
	public static String getBankName(JSONObject paymentTmpl, String bank, String paymentMethod){		 
	    JSONArray items = (JSONArray) paymentTmpl.get("items");
	    for(int j = 0 ; j < items.length(); j++ ){
	    	JSONObject item = (JSONObject)items.get(j);
	    	if( item.getString("title").equals("Payment Methods")){
	    		JSONArray payMethodItems = (JSONArray)item.get("items");
	    		for(int k = 0 ; k < payMethodItems.length(); k++ ){
		    		JSONObject payMethodItem = (JSONObject) payMethodItems.get(k);
		    		if(payMethodItem.has("id") && payMethodItem.get("id") instanceof String && payMethodItem.getString("id").equals(paymentMethod + "Block")){
		    			JSONArray blockItems  =  payMethodItem.getJSONArray("items");
		    			for(int l = 0; l < blockItems.length(); l++){
		    				JSONObject blockItem = blockItems.getJSONObject(l);
		    				if(blockItem.has("items") && blockItem.get("items") instanceof JSONArray){
		    					JSONArray blockItemItems  =  blockItem.getJSONArray("items");
        		    			for(int m = 0; m < blockItemItems.length(); m++){
        		    				JSONObject blockItemItem = blockItemItems.getJSONObject(m);
        		    				if(blockItemItem.has("id") && blockItemItem.getString("id").equals(paymentMethod+"BankNamePicker")){
        		    					JSONArray options  =  blockItemItem.getJSONArray("options");
                		    			for(int n = 0; n < options.length(); n++){
                		    				JSONObject option = options.getJSONObject(n);
                		    				if(option.has("value") && option.getString("value") instanceof String && option.getString("value").equals(bank)){
                		    					return bank = option.getString("title");  
                		    				}
                		    			}                   		    					
        		    				}
        		    			}            		    					
		    				}
		    				 
		    			}
	    			}
	    		}
	    	} 
	    }
	    return "";
	}
	
	public static String getPaymentMethodName(JSONObject paymentTmpl,String paymentMethod){	 
		JSONArray items = (JSONArray) paymentTmpl.get("items");
	    for(int j = 0 ; j < items.length(); j++ ){
	    	JSONObject item = (JSONObject)items.get(j);
	    	if( item.getString("title").equals("Payment Methods")){
	    		JSONArray payMethodItems = (JSONArray)item.get("items");
	    		for(int k = 0 ; k < payMethodItems.length(); k++ ){
		    		JSONObject payMethodItem = (JSONObject) payMethodItems.get(k);
		    		if(payMethodItem.has("type") && payMethodItem.getString("type").equals("12Box")){
		    			JSONArray _12BoxItems = (JSONArray)payMethodItem.get("items");
		    			for(int l =0 ; l<_12BoxItems.length();l++){
		    				JSONObject _12BoxItem = _12BoxItems.getJSONObject(l) ;
		    				if(_12BoxItem.getString("id").equals("initPayMethod")){
		    					JSONArray options  =  _12BoxItem.getJSONArray("options");
				    			for(int m = 0;m < options.length(); m++){	
				    				JSONObject option = options.getJSONObject(m);
				    				if(option.has("value") && option.getString("value") instanceof String && option.getString("value").equals(paymentMethod)){
				    					return option.getString("title");                    		    					
				    				}
				    			}
		    				}
		    				
		    			}
		    			
	    			}
	    		}
	    	} 
	    }
	    return "";
	}
    
	public static String yesNoConvert(String inputStr, String yesStr, String noStr){ 
	    if (inputStr.equals("Y")) 
	    	 return yesStr;
	    else if (inputStr.equals("N")) 
	    	return noStr;
	    else
	    	return " ";    	
    }
	
	 
	
	public JSONObject getDocValue(JSONArray rows, String idKey,  String id ){
		for(int i = 0; i<rows.length();i++){
			JSONObject row = rows.getJSONObject(i);
			JSONObject value = row.getJSONObject("value");
			if(value.has(idKey)&&value.get(idKey)instanceof String && value.getString(idKey).equals(id)){ 
				return value;
			}			
		}		 
		return null;
	}
	
	public static long dateStr2Long(String startDate, int hour, int min, int sec, int nanoSec){
	       
		try { 
			SimpleDateFormat sdfStart = new SimpleDateFormat(Constant.DATETIME_PATTERN_AS_ISOSTRING);  
			Date dtStartDate = sdfStart.parse(startDate);
	       
	        Calendar cal = Calendar.getInstance();
	        cal.setTimeZone(TimeZone.getTimeZone(Constant.ZONE_ID));
	        cal.setTime(dtStartDate);
	        int year = cal.get(Calendar.YEAR);
	        int month = cal.get(Calendar.MONTH)+1;
	        int day = cal.get(Calendar.DAY_OF_MONTH);
			 
			ZonedDateTime zdt1 = LocalDateTime.of(year, month,  day, hour, min, sec,nanoSec).atZone( ZoneId.of(Constant.ZONE_ID));			
			return zdt1.toInstant().toEpochMilli(); 
			
		} catch (ParseException e) {
			Log.debug("Exception in dateStr2Long:" + e );
		}
		return -1;		
 		  
	}
	
	


}
