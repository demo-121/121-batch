package com.eab.batch.report;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eab.common.Constant;
import com.eab.common.Function;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.RPT_SINGPOST_SUBMIT_TRX;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SingpostSubmissionReportJob implements Job {

	private final static SingpostSubmissionReportService singpostSubmissionReportService = new SingpostSubmissionReportService();
	private final static SingpostSubmissionReportProvider reportProvider = new SingpostSubmissionReportProvider();
	
	private final static DateTimeFormatter DATE_FORMAT_AXA_EXCEL_NAME = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_AXA_EXCEL_NAME);
	
	private final static DateTimeFormatter DATE_FORMAT_SQL = DateTimeFormatter.ofPattern(Constant.DATETIME_PATTERN_AS_SQL_DATE);
	
	private final static String FILE_NAME_PREFIX = "FC_SubmissionReport_";

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		StopWatch stopWatch = new StopWatch();

		stopWatch.start();
		startBatch();
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");

		stopWatch = null;
	}

	public static void startBatch() {
		Log.info("Start SingpostSubmissionReportJob");

		Log.debug("************************ Start SingpostSubmissionReportJob ****************************** ");
		RPT_SINGPOST_SUBMIT_TRX singPostReportDao = new RPT_SINGPOST_SUBMIT_TRX();
		String uuid = UUID.randomUUID().toString();
		try {
			boolean createResult = singPostReportDao.create(uuid);
			Log.debug("*** singPostReportDao.create=" + createResult);
		} catch (Exception ex) {
			Log.error(ex);
		}

		try {
			JSONObject lastCheckRecord = singPostReportDao.selectLastCheckPointTime();
			
			java.sql.Date fromTime = Function.convertJavaDateToSqlDate(new Date(0)); //Date time from beginning 
			
			if(lastCheckRecord != null) {
				Log.debug("*** lastCheckRecord=" + lastCheckRecord);
				if(lastCheckRecord.has("MAX_DT")) {
					String lastCheckTimeString = lastCheckRecord.get("MAX_DT").toString();
					
					ZonedDateTime zoneDate = ZonedDateTime.parse(lastCheckTimeString,
							DATE_FORMAT_SQL.withZone(ZoneId.of("UTC")));
					java.util.Date utilDate = java.util.Date.from ( zoneDate.toInstant() ) ;
					fromTime = Function.convertJavaDateToSqlDate(utilDate);
					
					Log.debug("*** fromTime=" + fromTime.toString());
				}
			}
			
			ZonedDateTime currentDate = Function.getDateUTCNow();
			java.sql.Date toTime = Function.covertZonedDateTimeToSqlDate(currentDate);

			JSONArray submittedCasesArray = singpostSubmissionReportService.submittedCasesToRLS(fromTime, toTime);
			Log.debug("**** submittedCasesArray=" + submittedCasesArray);

			if (submittedCasesArray != null) {
				int caseCount = submittedCasesArray.length();
				
				boolean updateSingpostCountFromLocalDB = singPostReportDao.updateSingpostCountFromLocalDB(uuid,
						fromTime, toTime, caseCount);
				Log.debug("*** updateSingpostCountFromLocalDB=" + updateSingpostCountFromLocalDB);
				
				String excelFileData = null;
				
				String reportPassword = singpostSubmissionReportService.generateSingpostReportPassword();
				
				if(caseCount>0) {

					ArrayList<String> eAppIdArrayList = singpostSubmissionReportService
							.extractApplicationIdArray(submittedCasesArray);

					if (eAppIdArrayList.size() > 0) {
						Log.debug("**** eAppIdArrayList.size()=" + eAppIdArrayList.size());
						JSONArray singpostResultJsonArray = singpostSubmissionReportService
								.getSingPostEappsOnCouchBase(eAppIdArrayList);
						Log.debug("**** singpostResultJsonArray=" + singpostResultJsonArray);

						if (singpostResultJsonArray != null) {
							int singpostCount = 0;
							singpostCount = singpostResultJsonArray.length();
							
							boolean updateSingpostCountFromCouchbase = singPostReportDao
									.updateSingpostCountFromCouchbase(uuid, singpostCount);
							Log.debug("*** updateSingpostCountFromCouchbase=" + updateSingpostCountFromCouchbase);
							
							if(singpostCount > 0) {

								ArrayList<String> agentCodeArray = singpostSubmissionReportService
										.extractAgentCodeArray(singpostResultJsonArray);
								
								int requestAgentCount = 0;
								int responseAgentCount = 0;
								
								requestAgentCount = agentCodeArray.size();
								JSONArray agentResultJsonArray = singpostSubmissionReportService
										.getAgentsOnCouchBase(agentCodeArray);

								if (agentCodeArray.size() > 0) {
									Log.debug("**** agentCodeArray.size()=" + agentCodeArray.size());

									if (agentResultJsonArray != null) {
										responseAgentCount = agentResultJsonArray.length();
//										Log.debug("**** agentResultJsonArray=" + agentResultJsonArray);

										boolean updateAgentCountFromCouchbase = singPostReportDao
												.updateAgentCountFromCouchbase(uuid, requestAgentCount, responseAgentCount);
//										Log.debug("*** updateAgentCountFromCouchbase=" + updateAgentCountFromCouchbase);

										HashMap<String, JSONObject> singPostAppsByAppIdMap = singpostSubmissionReportService
												.convertSingPostAppsToHashMapByAppId(singpostResultJsonArray);
										HashMap<String, JSONObject> mergedSubmissionDateMap = singpostSubmissionReportService
												.mergeSubmissionDateForSingPostApps(singPostAppsByAppIdMap,
														submittedCasesArray);
//										Log.debug("**** mergedSubmissionDateMap=" + mergedSubmissionDateMap);

										HashMap<String, JSONObject> mergedManagerNameMap = singpostSubmissionReportService
												.mergeManagerNameForSingPostApps(singPostAppsByAppIdMap, agentResultJsonArray);
//										Log.debug("**** mergedManagerNameMap=" + mergedManagerNameMap);

										JsonArray convertedJsonArray = singpostSubmissionReportService
												.converSingPostAppsMapToJsonArray(mergedManagerNameMap, eAppIdArrayList);
										Log.debug("**** convertedJsonArray=" + convertedJsonArray);

										String[][] excelRowData = singpostSubmissionReportService
												.parseToExcelDataArray(convertedJsonArray);

										if (excelRowData != null) {
											excelFileData = reportProvider.createExcelReportInBase64Format(excelRowData, reportPassword);
											
										} else {
											Log.error("ERROR - excelRowData for excel");
										}
									}else {
										Log.error("ERROR - agentResultJsonArray is null");
									}
								}else {
									Log.error("ERROR - agentCode array not extracted");
								}
								
							}else {
								//no singpost cases
								excelFileData = reportProvider.createEmptyReport(reportPassword);
							}	
						}else {
							Log.error("ERROR - singpostResultJsonArray is null");
						}
					}else {
						Log.error("ERROR - eAppId not extracted");
					}
				}else {
					//No submission 
					excelFileData = reportProvider.createEmptyReport(reportPassword);
				}
				
				if(excelFileData != null) {
					ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of(Constant.ZONE_ID));
					String convertedDateString = DATE_FORMAT_AXA_EXCEL_NAME.format(dateTime);

					String fileName = FILE_NAME_PREFIX + convertedDateString + ".xls";
					
					JsonObject reportJsonBody = singpostSubmissionReportService.createReportJsonBody(fileName, excelFileData);
					
					if(reportJsonBody != null) {
						HttpPost postRequest = singpostSubmissionReportService.createEmailPostRequest(reportJsonBody);
						Response output = HttpUtil.send(postRequest);
						
						boolean isCompleted = output.getStatus() == 204? true:false;
						Log.debug("**** Send Report=" + isCompleted);
						
						java.sql.Date emailTime = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
						boolean updateFromSendingEmail = singPostReportDao.updateFromSendingEmail(uuid, fileName, emailTime, isCompleted);
						Log.debug("**** updateFromSendingEmail=" + updateFromSendingEmail);
					}else {
						Log.error("ERROR - singpostSubmissionReportService.createReportJsonBody jsonBody is null");
					}
					
					JsonObject passwordJsonBody = singpostSubmissionReportService.createPasswordJsonBody(reportPassword);
					
					if(reportJsonBody != null) {
						HttpPost postRequest = singpostSubmissionReportService.createEmailPostRequest(passwordJsonBody);
						Response output = HttpUtil.send(postRequest);
						
						boolean isCompleted = output.getStatus() == 204? true:false;
						
						Log.debug("**** Send Report Password isCompleted=" + isCompleted);
						
						Log.debug("**** email status =" + output.getStatus());
						
						if(!isCompleted) {
							Log.debug("**** Fail to send report" + output.getEntity().toString());
						}

					}else {
						Log.error("ERROR - singpostSubmissionReportService.createPasswordJsonBody jsonBody is null");
					}
					
				}
			}
		} catch (Exception ex) {
			Log.error(ex);
		}

		Log.debug("************************ End SingpostSubmissionReportJob ****************************** ");
	}

}
