package com.eab.batch.report;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import com.eab.common.Constant;
import com.eab.common.Log;

public class SingpostSubmissionReportProvider {
	
	private final short HEADER_COLOR_INDEX = 57;
	
	private final String SHEET_NAME = "Report";
	
	private SimpleDateFormat DATE_FORMAT_SQL = new SimpleDateFormat(Constant.DATETIME_PATTERN_AS_SQL_DATE);

	private final String[] titles = { "S/N", "Submission Date", "PN", "Cust ID", "Cust Name", "FC", "CSM", "FSA Code",
			"Plan Name", "APE" };
	
	public String createEmptyReport(String password) throws Exception {
		
		return createExcelReportInBase64Format(new String[0][], password);
	}
	
	public String createExcelReportInBase64Format(String[][] data, String password) throws Exception {
		Log.info("Goto SingpostSubmissionReportProvider.createExcelReportInBase64Format");

		Biff8EncryptionKey.setCurrentUserPassword(password);
		HSSFWorkbook wb = new HSSFWorkbook();

		Map<String, HSSFCellStyle> styles = createStyles(wb);

		Sheet sheet = wb.createSheet(SHEET_NAME);

		// turn off gridlines
		sheet.setDisplayGridlines(false);
		sheet.setPrintGridlines(false);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);

		// the following three statements are required only for HSSF
		sheet.setAutobreaks(true);
		printSetup.setFitHeight((short) 1);
		printSetup.setFitWidth((short) 1);

		// the header row: centered text in 48pt font
		Row reportNameRow = sheet.createRow(0);
		reportNameRow.setHeightInPoints(12.75f);
		Cell nameCell = reportNameRow.createCell(0);
		nameCell.setCellValue("FC Submission Report");
		nameCell.setCellStyle(styles.get("cell_report_name"));
		
		Row headerRow = sheet.createRow(1);
		headerRow.setHeightInPoints(12.75f);
		for (int i = 0; i < titles.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(titles[i]);
			cell.setCellStyle(styles.get("header"));
		}
		
		// freeze the first 2 rows
		sheet.createFreezePane(0, 2);

		Row row;
		Cell cell;
		int rownum = 2;
		for (int i = 0; i < data.length; i++, rownum++) {
			
			row = sheet.createRow(rownum);
			if (data[i] == null)
				continue;

			for (int j = 0; j < data[i].length; j++) {
				
				cell = row.createCell(j);

				String styleName = "cell_normal";
				if(data[i][j] != null) {
					switch (j) {
					case 0:
						cell.setCellValue(Integer.parseInt(data[i][j]));
						break;
					case 1:
						String submittedSqlDateTime = data[i][j];
						
						DATE_FORMAT_SQL.setTimeZone(TimeZone.getTimeZone("UTC"));
						
						Date parsedDate = DATE_FORMAT_SQL.parse(submittedSqlDateTime);
						Calendar calendar = Calendar.getInstance();
						calendar.setTimeZone(TimeZone.getTimeZone(Constant.ZONE_ID));
						calendar.setTime(parsedDate);
						
						cell.setCellValue(calendar.getTime());
						styleName = "cell_normal_date";
						
						break;
					case 9:
						cell.setCellValue(Double.parseDouble(data[i][j]));
						styleName = "cell_number";
						break;
					default:
						cell.setCellValue(data[i][j]);
						break;
					}
				}
				
				cell.setCellStyle(styles.get(styleName));
			}
		}

		// //group rows for each phase, row numbers are 0-based
		// sheet.groupRow(4, 6);
		// sheet.groupRow(9, 13);
		// sheet.groupRow(16, 18);

		// set column widths, the width is measured in units of 1/256th of a character
		// width
		sheet.setColumnWidth(0, 256 * 6);
		sheet.setColumnWidth(1, 256 * 20);
		sheet.setColumnWidth(2, 256 * 20);
		sheet.setColumnWidth(3, 256 * 20);
		sheet.setColumnWidth(4, 256 * 30);
		sheet.setColumnWidth(5, 256 * 30);
		sheet.setColumnWidth(6, 256 * 30);
		sheet.setColumnWidth(7, 256 * 20);
		sheet.setColumnWidth(8, 256 * 35);
		sheet.setColumnWidth(9, 256 * 20);
		sheet.setZoom(100); // 100% scale

		// Write the output to a file

		// convert outputStream to base64 string
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		wb.write(outputStream);
		byte[] img64 = Base64.encodeBase64(outputStream.toByteArray());
		String b64Str = new String(img64);
		outputStream.close();

		wb.close();

		return b64Str;
	}

	/**
	 * create a library of cell styles
	 */
	private Map<String, HSSFCellStyle> createStyles(HSSFWorkbook wb) {
		Log.info("Goto SingpostSubmissionReportProvider.Map");
		
		Map<String, HSSFCellStyle> styles = new HashMap<>();
		DataFormat df = wb.createDataFormat();

		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HEADER_COLOR_INDEX, (byte) 204, (byte) 236, (byte) 255);// Header color

		HSSFCellStyle style = wb.createCellStyle();
		org.apache.poi.ss.usermodel.Font headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBold(true);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setVerticalAlignment(VerticalAlignment.CENTER);
		style.setFillForegroundColor(palette.getColor(HEADER_COLOR_INDEX).getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFont(headerFont);
		style.setWrapText(true);
		styles.put("header", style);

		org.apache.poi.ss.usermodel.Font normalFont = wb.createFont();
		normalFont.setFontHeightInPoints((short) 10);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setFont(normalFont);
		style.setWrapText(true);
		styles.put("cell_normal", style);

		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.CENTER);
		style.setWrapText(true);
		style.setDataFormat(df.getFormat(Constant.DATETIME_PATTERN_AS_AXA));
		styles.put("cell_normal_date", style);
		
		style = createBorderedStyle(wb);
		style.setAlignment(HorizontalAlignment.RIGHT);
		style.setWrapText(true);
		style.setDataFormat(df.getFormat("#.00"));
		styles.put("cell_number", style);
		
		style = wb.createCellStyle();
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setWrapText(false);
		style.setFont(normalFont);
		styles.put("cell_report_name", style);

		return styles;
	}

	private HSSFCellStyle createBorderedStyle(HSSFWorkbook wb) {
		Log.info("Goto SingpostSubmissionReportProvider.createBorderedStyle");
		
		BorderStyle thin = BorderStyle.THIN;
		short black = IndexedColors.BLACK.getIndex();

		HSSFCellStyle style = wb.createCellStyle();
		style.setBorderRight(thin);
		style.setRightBorderColor(black);
		style.setBorderBottom(thin);
		style.setBottomBorderColor(black);
		style.setBorderLeft(thin);
		style.setLeftBorderColor(black);
		style.setBorderTop(thin);
		style.setTopBorderColor(black);
		return style;
	}

}
