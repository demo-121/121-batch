package com.eab.batch.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.batch.report.PasswordGenerator.PasswordCharacterSet;
import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.BATCH_EMAIL_TEMPLATE;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.eab.dao.SYSTEM_PARAM;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SingpostSubmissionReportService {
	
	private final int MIN_PW_LENGTH = 8;
	private final int MAX_PW_LENGTH = 12;
	
	private static final char[] ALPHA_UPPER_CHARACTERS = { 'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    private static final char[] ALPHA_LOWER_CHARACTERS = { 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
    private static final char[] NUMERIC_CHARACTERS = { '0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9' };
    private static final char[] SPECIAL_CHARACTERS = { '~', '!', '@', '#',
            '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '|', ';', ',', '?' };
	
	private final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private final String SERVICE_PATH = "/email/sendEmail";
	
	private final String SINGPOST_APP_VIEW_NAME = "singpostApps";
	private final String AGENT_VIEW_NAME = "agentDetails";
	
	public JSONArray submittedCasesToRLS(java.sql.Date fromTime, java.sql.Date toTime) {
		Log.info("Goto SingpostSubmissionReportService.submittedCasesToRLS");
		
		BATCH_SUBMISSION_REQUEST submissionDao = new BATCH_SUBMISSION_REQUEST();
		
		
		JSONArray resultArray = null;
		
		try {
			resultArray = submissionDao.selectSubmittedCasesToRLS(fromTime,toTime);
		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return resultArray;
	}
	
	public JSONArray getSingPostEappsOnCouchBase(ArrayList<String> eAppIdArray) {
		Log.info("Goto SingpostSubmissionReportService.getSingPostEappsOnCouchBase");
		
		String[][] array = new String[eAppIdArray.size()][];
		for (int i = 0; i < eAppIdArray.size(); i++) {
			ArrayList<String> keyList = new ArrayList<String>();
			String eAppNo = eAppIdArray.get(i);
			keyList.add("01");
			keyList.add(eAppNo);
			
		    array[i] = keyList.toArray(new String[keyList.size()]);
		}
		
		return Constant.CBUTIL.getDocsByView(SINGPOST_APP_VIEW_NAME, array);
	}
	
	public JSONArray getAgentsOnCouchBase(ArrayList<String> agentCodeArray) {
		Log.info("Goto SingpostSubmissionReportService.getAgentsOnCouchBase");
		
		String[][] array = new String[agentCodeArray.size()][];
		for (int i = 0; i < agentCodeArray.size(); i++) {
			ArrayList<String> keyList = new ArrayList<String>();
			String agentCode = agentCodeArray.get(i);
			keyList.add("01");
			keyList.add("agentCode");
			keyList.add(agentCode);
			
		    array[i] = keyList.toArray(new String[keyList.size()]);
		}
		
		return Constant.CBUTIL.getDocsByView(AGENT_VIEW_NAME, array);
	}
	
	public ArrayList<String> extractApplicationIdArray(JSONArray submittedCasesToRLSArray){
		Log.info("Goto SingpostSubmissionReportService.extractApplicationIdArray");
		ArrayList<String> arrayList = new ArrayList<String>();
		
		for(Object item : submittedCasesToRLSArray) {
			JSONObject jsonObject = (JSONObject) item;
			String eAppNo = jsonObject.getString("EAPP_NO");
			
			arrayList.add(eAppNo);
		}
		
		return arrayList;
	}
	
	public ArrayList<String> extractAgentCodeArray(JSONArray applicationIdJSONArray){
		Log.info("Goto SingpostSubmissionReportService.extractAgentCodeArray");
		
		ArrayList<String> arrayList = new ArrayList<String>();
		
		for(Object item : applicationIdJSONArray) {
			JSONObject jsonObject = (JSONObject) item;
			if(jsonObject.has("value")) {
				JSONObject value = jsonObject.getJSONObject("value");
				
				if(value.has("aCode")) {
					String agentCode = value.getString("aCode");
					arrayList.add(agentCode);
				}
			}
		}
		
		return arrayList;
	}
	
	public HashMap<String, JSONObject> convertSingPostAppsToHashMapByAppId(JSONArray singPostEappsJsonArray) {
		Log.info("Goto SingpostSubmissionReportService.convertSingPostAppsToHashMapByAppId");
		
		HashMap<String, JSONObject> singPostAppsByAppIdMap = new HashMap<String, JSONObject>();
		
		for(Object item : singPostEappsJsonArray) {
			JSONObject jsonObject = (JSONObject) item;
			
			if(jsonObject.has("value") && jsonObject.has("id")) {
				JSONObject value = (JSONObject) jsonObject.get("value");
				
				String id = jsonObject.getString("id");
				singPostAppsByAppIdMap.put(id, value);
			}
		}
		
		return singPostAppsByAppIdMap;
	}
	
	public HashMap<String, JSONObject> mergeSubmissionDateForSingPostApps(HashMap<String, JSONObject> singPostAppsByAppIdMap, JSONArray submittedCasesToRLSArray) {
		Log.info("Goto SingpostSubmissionReportService.mergeSubmissionDateForSingPostApps");
		
		for(Object item : submittedCasesToRLSArray) {
			JSONObject jsonObject = (JSONObject) item;
			String EAPP_NO = jsonObject.getString("EAPP_NO");
			Object SUBMISSION_DATE_END = jsonObject.get("SUBMISSION_DATE_END");
			
			JSONObject singPostAppObject = singPostAppsByAppIdMap.get(EAPP_NO);
			
			if(singPostAppObject !=null) {
				singPostAppObject.put("date", SUBMISSION_DATE_END);
				singPostAppsByAppIdMap.put(EAPP_NO, singPostAppObject);
			}
		}
		
		return singPostAppsByAppIdMap;
	}
	
	public HashMap<String, JSONObject> mergeManagerNameForSingPostApps(HashMap<String, JSONObject> singPostAppsByAppIdMap, JSONArray agentResultJsonArray) {
		Log.info("Goto SingpostSubmissionReportService.mergeManagerNameForSingPostApps");
		
		HashMap<String, String> agentManagerNameByAgentCodeMap = new HashMap<String, String>();
		
		for(Object item : agentResultJsonArray) { //create a map that divided agent Manager Name By AgentCode
			JSONObject jsonObject = (JSONObject) item;
			
			if(jsonObject.has("value") && jsonObject.has("id")) {
				JSONObject value = (JSONObject) jsonObject.get("value");
				
				String agentCode = value.getString("agentCode");
				String manager = value.getString("manager");
				agentManagerNameByAgentCodeMap.put(agentCode, manager);
			}
		}
		
		for (String key : singPostAppsByAppIdMap.keySet()) {
			JSONObject singPostAppObject = singPostAppsByAppIdMap.get(key);
			
			if(singPostAppObject.has("aCode")) {
				String agentCode = singPostAppObject.getString("aCode");
				
				String manager = agentManagerNameByAgentCodeMap.get(agentCode);
				
				if(manager !=null)
				{
					singPostAppObject.put("manager", manager);
					singPostAppsByAppIdMap.put(key, singPostAppObject);
				}
			}
		}
		
		return singPostAppsByAppIdMap;
	}
	
	public JsonArray converSingPostAppsMapToJsonArray(HashMap<String, JSONObject> singPostAppsByAppIdMap, ArrayList<String> eAppIdArrayList) {
		Log.info("Goto SingpostSubmissionReportService.converSingPostAppsMapToJsonArray");
		
		JsonArray jsonArray = new JsonArray();
		
		for(String key: eAppIdArrayList) { // records order by eAppIdArrayList
			JSONObject singPostAppObject = singPostAppsByAppIdMap.get(key);
			
			if(singPostAppObject!=null) {
				String singPostAppObjString = singPostAppObject.toString();
				JsonObject jsonObject = Function.validateJsonFormatForString(singPostAppObjString);
				jsonArray.add(jsonObject);
			}
		}
		
		return jsonArray;
	}
	
	public String[][] parseToExcelDataArray(JsonArray singPostAppsJsonArray) {
		Log.info("Goto SingpostSubmissionReportService.parseToExcelDataArray");

		ArrayList<String[]> rowArrayList = new ArrayList<String[]>();
		int sequenceNum = 1;
		for (JsonElement item : singPostAppsJsonArray) {

//			Log.debug("***************  sequenceNum =" + sequenceNum + "     *************");

			JsonObject singPostJsonObject = item.getAsJsonObject();
//			Log.debug("*** singPostJsonObject =" + singPostJsonObject);

			String dateString = singPostJsonObject.has("date") ? singPostJsonObject.get("date").getAsString() : null;
			String policyNumberString = singPostJsonObject.has("policyNumber")
					? singPostJsonObject.get("policyNumber").getAsString()
					: null;
					
			Log.debug("***************  policyNumberString =" + policyNumberString + "     *************");
			String proposerId = singPostJsonObject.has("pId") ? singPostJsonObject.get("pId").getAsString() : null;
			String proposerName = singPostJsonObject.has("pName") ? singPostJsonObject.get("pName").getAsString()
					: null;
			String agentName = singPostJsonObject.has("aName") ? singPostJsonObject.get("aName").getAsString() : null;
			String managerName = singPostJsonObject.has("manager") ? singPostJsonObject.get("manager").getAsString()
					: null;
			String bankRefId = singPostJsonObject.has("bankRefId") ? singPostJsonObject.get("bankRefId").getAsString()
					: null;
			
			JsonObject planDetails = singPostJsonObject.has("planDetails") ? singPostJsonObject.get("planDetails").getAsJsonObject()
					: null;
			
			boolean isShield = false;

			// Plans
			if (singPostJsonObject.has("quotation") && singPostJsonObject.has("productCode")) {
				
				JsonObject quotationObject = singPostJsonObject.get("quotation").getAsJsonObject();
				
				double topupRegular = 0;
				double topupLumpsum = 0;
				
				JsonObject policyOptionsObject = quotationObject.has("policyOptions")?quotationObject.get("policyOptions").getAsJsonObject():null;
				
				if(policyOptionsObject !=null) {
					topupRegular = policyOptionsObject.has("rspAmount") && !policyOptionsObject.get("rspAmount").isJsonNull()? policyOptionsObject.get("rspAmount").getAsDouble(): 0;
					
					if(topupRegular !=0)
					{
						topupRegular*=12;//flexi protector only has monthly
					}
					
					topupLumpsum = policyOptionsObject.has("topUpAmt") && !policyOptionsObject.get("topUpAmt").isJsonNull()? policyOptionsObject.get("topUpAmt").getAsDouble(): 0;
				}
				
				if(quotationObject.has("quotType")) {
					String quotType =  quotationObject.get("quotType").getAsString();
					
					if(quotType !=null && quotType.equals("SHIELD")) {
						isShield = true;
					}
				}
				
				String paymentMode = quotationObject.has("paymentMode") ? quotationObject.get("paymentMode").getAsString()
						: null;
				
				String productCode = singPostJsonObject.get("productCode").getAsString();
				
				double totalApe = 0;
				String planName = null;
				
				JsonArray plansJsonArray = null;
				
				if(quotationObject.has("plans") && !isShield) {
					plansJsonArray = quotationObject.get("plans").getAsJsonArray();
					
					if(plansJsonArray != null) {
						for (int i = 0; i < plansJsonArray.size(); i++) {
							JsonElement planItem = plansJsonArray.get(i);
							JsonObject planJsonObject = planItem.getAsJsonObject();

							double singlePremium = 0;
							double annualizedRegularPremium = 0;
							double yearTax = 0;

							if (planJsonObject.has("premium")) {
								singlePremium = planJsonObject.get("premium").getAsDouble();
							}

							if (planJsonObject.has("yearPrem")) {
								annualizedRegularPremium = planJsonObject.get("yearPrem").getAsDouble();
							}
							
							if (planJsonObject.has("tax")) {
								JsonObject taxObject = planJsonObject.get("tax").getAsJsonObject();
								if(taxObject.has("yearTax")) {
									yearTax = taxObject.get("yearTax").getAsDouble();
								}
							}
							
							Log.debug("***policyNumber="+policyNumberString+", annualizedRegularPremium="+annualizedRegularPremium+", singlePremium="+singlePremium);

							
							double ape = calculateAPE(paymentMode, productCode, singlePremium, annualizedRegularPremium, yearTax,topupRegular,topupLumpsum);
							
							totalApe += ape;

							if (planJsonObject.has("covName") && i==0) { //Use basic plan name only
								JsonObject covName = planJsonObject.get("covName").getAsJsonObject();
								Log.debug("*** covName =" + covName);
								if (covName.has("en")) {
									planName = covName.get("en").getAsString();
									Log.debug("*** planName =" + planName);
								}
							}
						}
						
						double apeRoundOff = Math.round(totalApe * 100.0) / 100.0; // round up to 2 decimal places
						String apeRoundOffString = String.valueOf(apeRoundOff);
						
						String[] rowArray = createRowArray(String.valueOf(sequenceNum), dateString, policyNumberString, proposerId,
								proposerName, agentName, managerName, bankRefId, planName, apeRoundOffString);
						rowArrayList.add(rowArray);
						
						sequenceNum++;
					}
					
				}else if(planDetails !=null & isShield){
					
					if(planDetails.has("planList")) {
						plansJsonArray = planDetails.get("planList").getAsJsonArray();
					}
					
					if(plansJsonArray != null) {
						for (int i = 0; i < plansJsonArray.size(); i++) {
							JsonElement planItem = plansJsonArray.get(i);
							JsonObject planJsonObject = planItem.getAsJsonObject();

							double singlePremium = 0;
							double annualizedRegularPremium = 0;
							double yearTax = 0;

							if (planJsonObject.has("premium")) {
								singlePremium = planJsonObject.get("premium").getAsDouble();
							}

							if (planJsonObject.has("yearPrem")) {
								annualizedRegularPremium = planJsonObject.get("yearPrem").getAsDouble();
							}
							
							if(isShield && planJsonObject.has("planCode")) {
								String planCode = planJsonObject.get("planCode").getAsString();
								
								String[] shieldBasicPlanCodes = new String[] { "ASIMSA","ASIMSB", "ASIMSC" };
								boolean isShieldBasicPlan = Arrays.asList(shieldBasicPlanCodes).contains(planCode);
								
								Log.debug("*** isShieldBasicPlan="+isShieldBasicPlan);
								
								if(isShieldBasicPlan) {
									annualizedRegularPremium = planDetails.has("axaShield")? planDetails.get("axaShield").getAsDouble():0;
								}
								
							}
							
							// Paul confirms tax value is already included in premium
//							if (planJsonObject.has("tax")) {
//								JsonElement taxElement = planJsonObject.get("tax");
//								if(taxElement.isJsonObject()) {
//									JsonObject taxObject = taxElement.getAsJsonObject();
//									if(taxObject.has("yearTax")) {
//										yearTax = taxObject.get("yearTax").getAsDouble();
//									}
//								}
//							}
							
							Log.debug("***Shield policyNumber="+policyNumberString+", annualizedRegularPremium="+annualizedRegularPremium+", singlePremium="+singlePremium);
							
							double ape = calculateShieldAPE(annualizedRegularPremium, yearTax);

							if (planJsonObject.has("covName")) {
								JsonObject covName = planJsonObject.get("covName").getAsJsonObject();
								Log.debug("*** covName =" + covName);
								if (covName.has("en")) {
									planName = covName.get("en").getAsString();
									Log.debug("*** planName =" + planName);
								}
							}
							
							double apeRoundOff = Math.round(ape * 100.0) / 100.0; // round up to 2 decimal places
							String apeRoundOffString = String.valueOf(apeRoundOff);
							
							String[] rowArray = createRowArray(String.valueOf(sequenceNum), dateString, policyNumberString, proposerId,
									proposerName, agentName, managerName, bankRefId, planName, apeRoundOffString);
							rowArrayList.add(rowArray);
							
							sequenceNum++;
						}
					}
				}


			} else {
				Log.debug("***  no plans");
				String[] rowArray = createRowArray(String.valueOf(sequenceNum), dateString, policyNumberString, proposerId,
						proposerName, agentName, managerName, bankRefId, null, null);
				rowArrayList.add(rowArray);
				sequenceNum++;

			}

		}

		return convertArrayListTo2DStringArray(rowArrayList);
	}

	private String[][] convertArrayListTo2DStringArray(ArrayList<String[]> rowArrayList) {
		Log.info("Goto SingpostSubmissionReportService.convertArrayListTo2DStringArray");

		String[][] array = new String[rowArrayList.size()][];
		for (int i = 0; i < rowArrayList.size(); i++) {
			String[] stringArray = rowArrayList.get(i);
			array[i] = stringArray;
		}

		return array;
	}
	
	private double calculateShieldAPE(double annualizedRegularPremium, double yearTax) {
		Log.info("Goto SingpostSubmissionReportService.calculateShieldAPE");
		
		//Annual premium equivalent (APE)
		double ape = annualizedRegularPremium + yearTax;
		return ape;
	}

	private double calculateAPE(String paymentMode, String productCode, double singlePremium,
			double annualizedRegularPremium, double yearTax, double topupRegular, double topupLumpsum) {
		Log.info("Goto SingpostSubmissionReportService.calculateAPE");
		
		Log.debug("*** paymentMode="+paymentMode+", productCode="+productCode+", singlePremium="+singlePremium+",annualizedRegularPremium="+annualizedRegularPremium+", yearTax="+yearTax);

		// 100% Annualized Regular Premium + 10% Top-up Regular + 10% Single Premium +
		// 10% Top-up Lump Sum

		double singlePremiumInTenPercent = 0;
		double topupRegularInTenPercent = topupRegular!=0?topupRegular*0.1:0; // + 10% Top-up Regular
		double topupLumpSumInTenPercent = topupLumpsum!=0?topupLumpsum*0.1:0; // + 10% Top-up lump sum

		if (paymentMode !=null) { 

			if(paymentMode.equals("L")) {
				// Inspire Duo plan only has Single Premium. Thus, we only need "premium"
				annualizedRegularPremium = 0; // "yearPrem" is a value by regular premium. Hence, it's not applicable for Single Premium case 
				singlePremiumInTenPercent = singlePremium * 0.1;
			}else {
				// For other product, we need "yearPrem" for regular premium
				singlePremiumInTenPercent = 0; // should ignore Single Premium value
			}
		} else {
			Log.error("ERROR - Missing paymentMode value");
		}
		
		//Annual premium equivalent (APE)
		double ape = annualizedRegularPremium + topupRegularInTenPercent + singlePremiumInTenPercent
				+ topupLumpSumInTenPercent + yearTax;
		
		return ape;
	}

	private String[] createRowArray(String sequenceNumString, String dateString, String policyNumberString,
			String proposerId, String proposerName, String agentName, String managerName, String bankRefId,
			String planName, String annualizedPremium) {
		Log.info("Goto SingpostSubmissionReportService.createRowArray");
		
		ArrayList<String> rowList = new ArrayList<String>();

		rowList.add(sequenceNumString);
		rowList.add(dateString);
		rowList.add(policyNumberString);
		rowList.add(proposerId);
		rowList.add(proposerName);
		rowList.add(agentName);
		rowList.add(managerName);
		rowList.add(bankRefId);
		rowList.add(planName);
		rowList.add(annualizedPremium);

		return rowList.toArray(new String[rowList.size()]);
	}

	public JsonObject createReportJsonBody(String fileName, String fileDataBase64Format) throws Exception {
		Log.info("Goto SingpostSubmissionReportService.createReportJsonBody");
		
		JsonObject jsonBody = null;
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String toEmail = eSysParam.selectSysValue("REPORT_RLS_SINGPOST_TO");
			String fromEmail = eSysParam.selectSysValue("REPORT_RLS_SINGPOST_FROM");
			
			Log.debug("*** toEmail="+toEmail+", fromEmail="+fromEmail);
			
			BATCH_EMAIL_TEMPLATE emailTemplateJson= new BATCH_EMAIL_TEMPLATE();
			
			JSONObject templateJsonObject = emailTemplateJson.selectEmailTemplate("REPORT_RLS_SINGPOST");
			
			String title = templateJsonObject.has("MAIL_SUBJ")? templateJsonObject.getString("MAIL_SUBJ"):null;
			String content = templateJsonObject.has("MAIL_CONTENT")? templateJsonObject.getString("MAIL_CONTENT"):null;
			
			if(toEmail !=null && fromEmail !=null && title !=null && content !=null) {
				JsonObject fileJson = new JsonObject();
				fileJson.addProperty("fileName", fileName);
				fileJson.addProperty("data", fileDataBase64Format);

				JsonArray attachments = new JsonArray();
				attachments.add(fileJson);

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content",content);
				jsonBody.add("attachments", attachments);
			}else {
				Log.error("ERROR - Report - toEmail, fromEmail, title, content have null");
			}
			
		}catch(Exception ex){
			Log.error(ex);
		}

		return jsonBody;
	}

	public HttpPost createEmailPostRequest(JsonObject jsonBody) throws Exception {
		Log.info("Goto SingpostSubmissionReportService.createEmailPostRequest");

		String emailURL = DOMAIN + SERVICE_PATH;

		HttpPost request = new HttpPost(emailURL);
		request.addHeader("Content-Type", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}
	
	public JsonObject createPasswordJsonBody(String password) throws Exception {
		Log.info("Goto SingpostSubmissionReportService.createPasswordJsonBody");
		
		JsonObject jsonBody = null;
		
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String toEmail = eSysParam.selectSysValue("REPORT_RLS_SINGPOST_TO");
			String fromEmail = eSysParam.selectSysValue("REPORT_RLS_SINGPOST_FROM");
			
			Log.debug("*** toEmail="+toEmail+", fromEmail="+fromEmail);
			
			BATCH_EMAIL_TEMPLATE emailTemplateJson= new BATCH_EMAIL_TEMPLATE();
			
			JSONObject templateJsonObject = emailTemplateJson.selectEmailTemplate("REPORT_RLS_SINGPOST_PW");
			
			String title = templateJsonObject.has("MAIL_SUBJ")? templateJsonObject.getString("MAIL_SUBJ"):null;
			String content = templateJsonObject.has("MAIL_CONTENT")? templateJsonObject.getString("MAIL_CONTENT"):null;
			
			password = StringEscapeUtils.escapeJson(password);
			
			content = content.replace("[password]", password);//password
			
			if(toEmail !=null && fromEmail !=null && title !=null && content !=null) {

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content",content);
			}else {
				Log.error("ERROR - Password - toEmail, fromEmail, title, content have null");
			}
			
		}catch(Exception ex){
			Log.error(ex);
		}


		return jsonBody;
	}
	
	public String generateSingpostReportPassword() {
		Set<PasswordCharacterSet> values = new HashSet<PasswordCharacterSet>(EnumSet.allOf(SummerCharacterSets.class));
        PasswordGenerator pwGenerator = new PasswordGenerator(values, MIN_PW_LENGTH, MAX_PW_LENGTH);
        
        char[] c = pwGenerator.generatePassword();
        
        return String.valueOf(c);
	}
	
	
	private enum SummerCharacterSets implements PasswordCharacterSet {
        ALPHA_UPPER(ALPHA_UPPER_CHARACTERS, 1),
        ALPHA_LOWER(ALPHA_LOWER_CHARACTERS, 1),
        NUMERIC(NUMERIC_CHARACTERS, 1),
        SPECIAL(SPECIAL_CHARACTERS, 1);

        private final char[] chars;
        private final int minUsage;

        private SummerCharacterSets(char[] chars, int minUsage) {
            this.chars = chars;
            this.minUsage = minUsage;
        }

        @Override
        public char[] getCharacters() {
            return chars;
        }

        @Override
        public int getMinCharacters() {
            return minUsage;
        }
    }

}
