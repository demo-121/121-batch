package com.eab.batch.submittowfineip;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;


import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.eab.common.Constant;
import com.eab.common.ConstantSubmission;
import com.eab.common.Log;
import com.eab.common.WfiRlsSubmissionUtil;
import com.eab.couchbase.CBUtil;
import com.eab.dao.BATCH_LOG;
import com.eab.dao.BATCH_SUBMISSION_REQUEST;
import com.eab.dao.EAPP_MST;
import com.eab.dao.SYSTEM_PARAM;
import com.eab.dao.WFI_EIP_EXCEPTION;
import com.eab.dao.WFI_TITLE_TYPE_MAPPING;
import com.eab.database.jdbc.DBAccess;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.eab.common.EnvVariable;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.eab.common.Function;
import com.eab.object.WFI_EASE_MAPPING;
import com.eab.rest.ease.email.EmailNotificationHelper;

public class SubmitToWfinEip implements Job {
	public static final String mimeType = "application/pdf";
	public static final DateTimeFormatter DATE_FORMAT_ISO8601 = DateTimeFormatter
			.ofPattern(Constant.DATETIME_PATTERN_AS_ISO8601);
	private static final String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private static final int TITLE = 1;
	private static final int TYPE = 2;
	private ArrayList<WFI_EASE_MAPPING> wfiEaseMapping;
	private static final String OP_MODE = "OP_MODE";
	Function func = new Function();
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		StopWatch stopWatch = new StopWatch();
		
		stopWatch.start();
		String tempValue = "M";
		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			tempValue = eSysParam.selectSysValue(OP_MODE);
		} catch (Exception e) {
			Log.error(e);
		}
		if (tempValue.equalsIgnoreCase("O")){
			startBatch();
		} else {
			Log.info("DB OP_MODE in SYSTEM_PARAM is Maintenance Mode");
		}
		
		stopWatch.stop();
		Log.info("Batch Job: " + this.getClass().getName() + " | Elapsed time: " + stopWatch.getTime() + " mills.");
		
		stopWatch = null;
	}
	
	private String handleEASEnAXAMapping(int type, String key, boolean isOtherDoc){
		String result = "";
		int findingIndex = -1;
		int othersIndex = -1;
		int resultIndex = -1;
		if (isOtherDoc) {
			// Return the user entered name to submit
			for (int i = 0; i < this.wfiEaseMapping.size(); i++) {
				WFI_EASE_MAPPING temp = this.wfiEaseMapping.get(i);
				
				if (temp.getEaseId().equalsIgnoreCase("OTHERS")){
					othersIndex = i;
				}
			}
			switch (type) {
				case TITLE:
					if (key != null){
						key = key.trim().replaceAll("\\s+", "");
					}
					result = key;
					break;
				case TYPE:
					result = this.wfiEaseMapping.get(othersIndex).getSgType();
					break;
			}
		} else {
			for (int i = 0; i < this.wfiEaseMapping.size(); i++) {
				WFI_EASE_MAPPING temp = this.wfiEaseMapping.get(i);
				
				if (temp.getEaseId().equalsIgnoreCase(key)) {
					findingIndex = i;
				}
				if (temp.getEaseId().equalsIgnoreCase("OTHERS")){
					othersIndex = i;
				}
			}
			resultIndex = (findingIndex == -1) ? othersIndex : findingIndex;
			switch (type) {
				case TITLE: 
					result = this.wfiEaseMapping.get(resultIndex).getSgTitle();
					//Remove all Tile Space and hypens
					if (result != null){
						result = result.trim().replaceAll("\\s+", "");
					}
					break;
				case TYPE:
					result = this.wfiEaseMapping.get(resultIndex).getSgType();
					break;
			}
		}
		
		return result;
	
	}
	
	public void startBatch() {
		Log.info("Submission to WFI and EIP");
		int bNo=0;
		int bNoforSubWfi = 0;
		int successNumber = 0;
		int failNumber = 0;
		int recordToTake = 100;
		String policyNum = "";
		try {
			
			BATCH_SUBMISSION_REQUEST subReq = new BATCH_SUBMISSION_REQUEST();
			JSONObject notoExecute = subReq.singlerecordquerySQL("SELECT COUNT(EAPP_NO) as EAPP_NO_COUNT from batch_submission_request where taken_by_batch_no is null");
			
			if (notoExecute.getInt("EAPP_NO_COUNT") != 0){
				
				BATCH_LOG bLog = new BATCH_LOG();
				
				bNo = bLog.selectSeq();
				Date curDate = new Date();
				boolean resultPreSub = bLog.create(bNo, "JOB_BATCH_SUBMIT_TO_WFI", "P", curDate, notoExecute.getInt("EAPP_NO_COUNT"));
				
				Log.debug("Start to update all record to have token");
				bNoforSubWfi = subReq.selectSeq();

				subReq.updateRecordtoTake(bNo, recordToTake);
				
				Log.info("batch job number " + bNo);
				
				

				JSONArray recordToExecute = subReq.querySQL("SELECT EAPP_NO, to_char( cast(CREATE_DATE as timestamp) at time zone 'UTC', 'yyyy-MM-dd\"T\"hh24:mi:ss\"+00:00\"') as CREATE_DATE from batch_submission_request where taken_by_batch_no = " + bNo);
				String eappId = null;
				
				ZonedDateTime createDate;
				
				WFI_TITLE_TYPE_MAPPING wfiMap = new WFI_TITLE_TYPE_MAPPING();
				this.wfiEaseMapping = wfiMap.enquiryMapping();

				Log.info("No. of records token in this batch job " + recordToExecute.length());
				
				for (int i = 0; i < recordToExecute.length(); i++){
					try {
						eappId = recordToExecute.getJSONObject(i).getString("EAPP_NO");			
						
								
						
						Log.debug("Enter the for lop and start to check bundle");

						JSONObject eappDoc = Constant.CBUTIL.getDoc(eappId);
						if (eappDoc != null)  {
							
							Log.debug("Going to submit WIF");
							policyNum = eappDoc.getString("policyNumber");
							
							JSONObject quoDoc = Constant.CBUTIL.getDoc(eappDoc.getString("quotationDocId"));
							JSONObject bundleDoc = Constant.CBUTIL.getDoc(eappDoc.getString("bundleId"));
							JSONObject approvalDoc = Constant.CBUTIL.getDoc(eappDoc.getString("policyNumber"));
							
							ZonedDateTime zonedUTCnow = Function.getDateUTCNow();
							
							createDate = ZonedDateTime.parse(recordToExecute.getJSONObject(i).getString("CREATE_DATE"), DateTimeFormatter.ISO_DATE_TIME);	
							
							Log.debug("UTC now: " + zonedUTCnow.toString());
							Log.debug("Create Date of current Record: " + createDate.toString());
							
							boolean allDependentPass = WfiRlsSubmissionUtil.getAllDependentStatus(eappId, eappDoc, quoDoc, ChronoUnit.MINUTES.between(createDate, zonedUTCnow));
							
							// Not ready for submission, rollback and retry later
							if (allDependentPass == false) {
								//Update db field 'TAKEN_BY_BATCH_NO' to null for retry later
								subReq.rollbackForResubmission(eappId);
								continue;
							}
							
							if (bundleDoc != null) {
								
								boolean subToWif = false;
								boolean subToEip = false;
								
								try {
									
									EAPP_MST eappMst = new EAPP_MST();
									Function func = new Function();
									JsonObject appDoc = func.transformObject(eappDoc);
									JsonObject bDoc = func.transformObject(bundleDoc);
									String appStatus = "";
									JsonArray applicationArray = new JsonArray();
									if (bDoc.get("applications") != null && bDoc.get("applications").getAsJsonArray() != null){
										applicationArray = bDoc.get("applications").getAsJsonArray();
									}
									
									for (int k =0; k< applicationArray.size(); k++){
										JsonObject obj = applicationArray.get(k).getAsJsonObject();
										String tempAppId = func.transformEleToString(obj.get("applicationDocId"));
										if (tempAppId.equalsIgnoreCase(eappId)){
											appStatus = func.transformEleToString(obj.get("appStatus"));
										}
									}
									eappMst.create(func.transformEleToString(appDoc.get("id")), 
											func.transformEleToString(func.getAsPath(appDoc.getAsJsonObject(), "quotation/agent/agentCode")), 
											func.transformEleToString(appDoc.get("policyNumber")), 
											func.transformEleToString(appDoc.get("bundleId")), 
											func.transformEleToString(appDoc.get("applicationStartedDate")), 
											func.transformEleToString(appDoc.get("applicationSignedDate")),
											appStatus, func.transformEleToString(func.getAsPath(appDoc.getAsJsonObject(), "payment/paymentMode")),  "SUBMIT_TO_WFI_EIP_BATCH");
									
									subToWif = submitToWfi(eappId, eappDoc, quoDoc, bundleDoc, approvalDoc, policyNum, bNo);

									Log.info("Submit to WFI Job Success: " + subToWif);
									
									if(!subToWif) { // Notify AXA for fail case
										EmailNotificationHelper.sendErrorNotificationWFI(policyNum); // Notify AXA for fail case of WFI
									}
									
								}catch (Exception e) {
									Log.error(e);
									subReq.update(eappId, 1, "F");
									
									EmailNotificationHelper.sendErrorNotificationWFI(policyNum); // Notify AXA for fail case of WFI
								}
								
								try {
									String eipPath;
									if (approvalDoc == null) {
										eipPath = eappDoc.getString("id");
									} else {
										eipPath = eappDoc.getString("policyNumber");
									}
									boolean isShield = quoDoc.has("quotType") && quoDoc.getString("quotType").equalsIgnoreCase("SHIELD");
									subToEip = submitToEIP(eipPath, policyNum, bNo, isShield);
									
									Log.info("Submit to EIP Job Success: " + subToEip);
									
									if(!subToEip) { // Notify AXA for fail case
										EmailNotificationHelper.sendErrorNotificationRLS(policyNum); // Notify AXA for fail case of RLS
									}

								}catch (Exception e) {
									Log.error(e);
									subReq.update(eappId, 1, "F");
									
									EmailNotificationHelper.sendErrorNotificationRLS(policyNum); // Notify AXA for fail case of RLS
								}
								
								if (subToWif && subToEip) {
									subReq.update(eappId, 1, "C");
								} else {
									subReq.update(eappId, 1, "F");
								}
								
							}
						}	
					} catch (Exception e) {
						Log.error(e);
						subReq.update(eappId, 1, "F");
						
						if(!policyNum.equals("")) {
							EmailNotificationHelper.sendErrorNotificationWFI(policyNum); // Notify AXA for fail case of WFI
							EmailNotificationHelper.sendErrorNotificationRLS(policyNum); // Notify AXA for fail case of RLS
						}else {
							EmailNotificationHelper.sendErrorNotificationWFI(" - "); // Notify AXA for fail case of WFI
							EmailNotificationHelper.sendErrorNotificationRLS(" - "); // Notify AXA for fail case of RLS
						}
					}
				}	

				Log.debug("Start to mark batch_log record complete");
				bLog.update(bNo, successNumber, "C", "BATCH");
			} else {
				return;
			}
			
		} catch (Exception ex) {
			Log.error(ex);
			try {
				BATCH_LOG bLog = new BATCH_LOG();
				bLog.update(bNo, successNumber, "F", "BATCH");
				
				WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
				weException.create(bNo, policyNum, convertExceptionToClob(ex));
			} catch (Exception dbEx){
				Log.error(dbEx);
			}
			
			if(!policyNum.equals("")) {
				EmailNotificationHelper.sendErrorNotificationWFI(policyNum); // Notify AXA for fail case of WFI
				EmailNotificationHelper.sendErrorNotificationRLS(policyNum); // Notify AXA for fail case of RLS
			}else {
				EmailNotificationHelper.sendErrorNotificationWFI(" - "); // Notify AXA for fail case of WFI
				EmailNotificationHelper.sendErrorNotificationRLS(" - "); // Notify AXA for fail case of RLS
			}
			
		}
		Log.info("Complete Submission of WFI and EIP");
	}
	
	private boolean submitToEIP(String eipPath, String policyNum, int batchNo, boolean isShield) {
		String resp = "";
		try {
			Log.info("Start to get json from internal eip api");
			
			String recordApplicationRequest = callgetApi(DOMAIN + "/submission/" + eipPath);
			
			
			JsonParser jsonParser = new JsonParser();
			JsonObject request = (JsonObject)jsonParser.parse(recordApplicationRequest);			
			
			JsonElement returnObj = request.get("success");
			
			
			if (returnObj == null) {
				JsonObject bodyToEip = new JsonObject();
				
				bodyToEip.add("recordApplicationRequest", request.get("recordApplicationRequest"));
				if (isShield) {
					bodyToEip.addProperty("lob", "Health");
				} else {
					bodyToEip.addProperty("lob", "Life");
				}
				
				Log.info("Start to call api to send data to AXA");

				resp = callpostApi(bodyToEip, DOMAIN + "/application");
				
				Function f = new Function();
				boolean result = f.handleResponse(resp, "asyncAppResponseStatus");
				if (!result) {
					WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
					weException.create(batchNo, policyNum, resp);
				}
				return result;
			} else 	{
				WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
				weException.create(batchNo, policyNum, request.toString());
				return false;
			} 
			
		} catch (Exception e) {
			Log.error(e);
			WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
			try {
				weException.create(batchNo, policyNum, convertExceptionToClob(e).concat(resp));
			} catch (Exception creRecException) {
				Log.error(creRecException);
			}
			return false;
		}
	}
	
	private boolean submitToWfi (String eAppid, JSONObject eappDoc, JSONObject quoDoc, JSONObject bundleDoc, JSONObject approvalDoc, String policyNum, int batchNo) {
		try {
			Log.info("Submit to WFI by internal eAPP");
			
			Function func = new Function();
			JsonObject qDoc = func.transformObject(quoDoc);
			String dealerGroup = func.transformEleToString(func.getAsPath(qDoc.get("agent"), "dealerGroup"));
			
			JsonObject channelDoc = func.transformObject(Constant.CBUTIL.getDoc("channels"));
			
			String channelType = func.transformEleToString(func.getAsPath(channelDoc, "channels/" + dealerGroup + "/type"));
			
			String approvalDocId = "";
			String appDocId = "";
			String appFormAttachmentId = "";
			JSONObject parenteAppDoc = null;
			boolean isShield = false;
			
			if (quoDoc.has("quotType") && quoDoc.getString("quotType").equalsIgnoreCase("SHIELD")) {
				approvalDocId = (approvalDoc != null) ? approvalDoc.getString("masterApprovalId") : "";
				appDocId = eappDoc.getString("parentId");
				appFormAttachmentId = eappDoc.getString("appFormAttachmentId");
				parenteAppDoc = Constant.CBUTIL.getDoc(appDocId);
				isShield = true;
			} else {
				approvalDocId = eappDoc.getString("policyNumber");
				appDocId = eAppid;
				appFormAttachmentId = ConstantSubmission.EAPP_FN;
			}
			
//			boolean submiteApp = submitSysDoc(appDocId, eappDoc, quoDoc, ConstantSubmission.EAPP_FN, policyNum, batchNo);
			boolean submiteApp = submitSysDoc(appDocId, eappDoc, quoDoc, appFormAttachmentId, policyNum, batchNo, isShield, true, ConstantSubmission.EAPP_FN, parenteAppDoc);
			
			boolean submitBI = submitSysDoc(quoDoc.getString("id"), eappDoc, quoDoc, ConstantSubmission.BI_FN, policyNum, batchNo, isShield, false, ConstantSubmission.BI_FN, parenteAppDoc);
			
			boolean submitFNA = true;
			
			if (!channelType.equalsIgnoreCase("FA")) {
				submitFNA = submitSysDoc(bundleDoc.getString("id"), eappDoc, quoDoc, ConstantSubmission.FNAREPORT_FN, policyNum, batchNo, isShield, false, ConstantSubmission.FNAREPORT_FN, parenteAppDoc);
			}

			boolean submitSupervisorApproval = true;
			boolean submitFAFirmComment = true;
			boolean submitJFWForm = true;
			
			if (approvalDoc != null) {
				submitSupervisorApproval = submitSysDoc(approvalDocId, eappDoc, quoDoc, ConstantSubmission.SUPERVISOR_EAPPROVAL, policyNum, batchNo, isShield, false, ConstantSubmission.SUPERVISOR_EAPPROVAL, parenteAppDoc);
				
				submitFAFirmComment = true;

				if (func.transformEleToBoolean(func.transformObject(approvalDoc).get("isFACase"))) {
					submitFAFirmComment = submitSysDoc(approvalDocId, eappDoc, quoDoc, ConstantSubmission.FAFIRM_COMMENT, policyNum, batchNo, isShield, false, ConstantSubmission.FAFIRM_COMMENT, parenteAppDoc);
				}
				
				submitJFWForm = subApprovalDocs(approvalDocId, eappDoc, quoDoc, policyNum, batchNo, isShield, parenteAppDoc);
			}
			
			boolean submiteOthers = subAppAllAtts(appDocId, eappDoc, quoDoc, policyNum, batchNo, isShield, parenteAppDoc);
				
			if(submiteApp == false || submitBI == false || submitFNA == false || submiteOthers ==false || submitJFWForm == false || submitSupervisorApproval == false || submitFAFirmComment == false) {
				return false;
			}else {
				return true;
			}
		}catch (Exception e) {
			Log.error(e);
			WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
			try {
				weException.create(batchNo, policyNum, convertExceptionToClob(e));
			} catch (Exception e1) {
				Log.error(e1);
			}
			return false;
		}
	}
	
	private boolean subAppAllAtts(String eappId, JSONObject eappDoc, JSONObject quoDoc, String policyNum, int batchNo, boolean isShield, JSONObject parentAppDoc) throws Exception {
		JsonParser jsonParser = new JsonParser();
		JsonObject appDoc = (JsonObject)jsonParser.parse(eappDoc.toString());
		boolean pBoolean = true;
		boolean pInsured = true;
		boolean pProposer = true;
		
		if (appDoc.get("supportDocuments") != null && appDoc.getAsJsonObject("supportDocuments").getAsJsonObject("values") != null && appDoc.getAsJsonObject("supportDocuments").getAsJsonObject("values").getAsJsonObject("policyForm") != null){
			pBoolean = sendBySection(appDoc.getAsJsonObject("supportDocuments").getAsJsonObject("values").getAsJsonObject("policyForm"), eappId, eappDoc, quoDoc, policyNum, batchNo, isShield, parentAppDoc);
			pInsured = sendBySection(appDoc.getAsJsonObject("supportDocuments").getAsJsonObject("values").getAsJsonObject("insured"), eappId, eappDoc, quoDoc, policyNum, batchNo, isShield, parentAppDoc);
			pProposer = sendBySection(appDoc.getAsJsonObject("supportDocuments").getAsJsonObject("values").getAsJsonObject("proposer"), eappId, eappDoc, quoDoc, policyNum, batchNo, isShield, parentAppDoc);
		}
		
		if(pBoolean == false || pInsured == false || pProposer == false) {
			return false;
		}else {
			return true;
		}
		
	}
	
	private boolean subApprovalDocs(String approvalId, JSONObject eappDoc, JSONObject quoDoc, String policyNum, int batchNo, boolean isShield, JSONObject parentAppDoc) throws Exception {
		JSONObject approvalDoc = Constant.CBUTIL.getDoc(approvalId);
		JsonParser jsonParser = new JsonParser();
		JsonObject subArr = (JsonObject)jsonParser.parse(approvalDoc.toString());
		
		return submitByArrary(subArr.getAsJsonArray("jfwFileName"), approvalId, eappDoc, quoDoc, "jfwFileName", "type", "attId", false, policyNum, batchNo, isShield, parentAppDoc);
	}
	
	private boolean sendBySection(JsonObject sectionDoc, String eappId, JSONObject eappDoc, JSONObject quoDoc, String policyNum, int batchNo, boolean isShield, JSONObject parentAppDoc) throws Exception {
		Set<Entry<String, JsonElement>> entrySet = sectionDoc.entrySet();
		boolean result = true;
		boolean tempResult = true;
		for(Map.Entry<String,JsonElement> entry : entrySet){
			if (!entry.getKey().equalsIgnoreCase("sysDocs") && !entry.getKey().equalsIgnoreCase("otherDoc")) {
				tempResult = sendDiffSectionDoc(sectionDoc.getAsJsonObject(entry.getKey()), eappId, eappDoc, quoDoc, false, policyNum, batchNo, isShield, parentAppDoc);
			} else if (entry.getKey().equalsIgnoreCase("otherDoc")){
				tempResult = sendDiffSectionDoc(sectionDoc.getAsJsonObject(entry.getKey()).getAsJsonObject("values"), eappId, eappDoc, quoDoc, true, policyNum, batchNo, isShield, parentAppDoc);
			}
			
			if (tempResult == false) {
				result = false;
			}
		}
		return result;
	}
	
	private boolean sendDiffSectionDoc(JsonObject fileInfoArr, String eappId, JSONObject eappDoc, JSONObject quoDoc, boolean isOtherDoc, String policyNum, int batchNo, boolean isShield, JSONObject parentAppDoc) throws Exception{
		JsonArray files;
		
		String easeAxaMappingKey ="";
		Set<Entry<String, JsonElement>> entrySet = fileInfoArr.entrySet();
		
		boolean result = true;
		for(Map.Entry<String,JsonElement> entry : entrySet){
			
			if (entry.getKey()!= null && entry.getKey().lastIndexOf("_") > -1 && isOtherDoc) {
				easeAxaMappingKey = entry.getKey().substring(0, entry.getKey().lastIndexOf("_"));
			} else {
				easeAxaMappingKey = entry.getKey();
			}
			
			files = fileInfoArr.getAsJsonArray(entry.getKey());
			
			if(submitByArrary(files, eappId, eappDoc, quoDoc, easeAxaMappingKey, "fileType", "id", isOtherDoc, policyNum, batchNo, isShield, parentAppDoc) == false){
				result = false;
			}
		}
		
		return result;
		
	}
	
	private boolean submitByArrary(JsonArray files, String docId, JSONObject eappDoc, JSONObject quoDoc, String easeAxaMappingKey, String fileTypeId, String attId, boolean isOtherDoc, String policyNum, int batchNo, boolean isShield, JSONObject parentAppDoc) throws Exception{
		boolean result = true;
		try {
			Function func = new Function();
			JsonObject valueObj;
			ArrayList<String> pdfBase64Str = new ArrayList<String>();
			String valueStr = "";
			ArrayList<String> base64Strs = new ArrayList<String>();
			ArrayList<String> filesType = new ArrayList<String>();
			
			boolean individualResult;
			if (files != null && files.size() > 0) {
				for (int i = 0; i < files.size(); i++){

					valueObj = files.get(i).getAsJsonObject();
					filesType.add(valueObj.get(fileTypeId).getAsString());
					
					valueStr = getAttinBase64(docId, valueObj.get(attId).getAsString());

					if (valueStr.indexOf("base64") != -1){
						valueStr = valueStr.substring(valueStr.indexOf("base64") + 6, valueStr.length());
					}
					base64Strs.add(valueStr);

				}
				
				pdfBase64Str = func.getContentToSend(base64Strs, filesType);
				for (int i=0; i < pdfBase64Str.size(); i++){
					individualResult = submitDoc(pdfBase64Str.get(i), eappDoc, quoDoc, easeAxaMappingKey, isOtherDoc, policyNum, batchNo, isShield, parentAppDoc);
					if (individualResult == false) {
						result = individualResult;
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
			WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
			try {
				weException.create(batchNo, policyNum, convertExceptionToClob(e));
			} catch (Exception e1) {
				Log.error(e1);
			}
			return false;
		}
		
		return result;
	}
	
	private boolean submitDoc(String pdfBase64, JSONObject eappDoc, JSONObject quoDoc, String docName, boolean isOtherDoc, String policyNum, int batchNo, boolean isShield, JSONObject parentAppDoc){
		try {
			Log.info("Submit Doc Name: " + docName);
			
			JsonObject bodyToWfi;
			
			String priDocFlag = "NO";
			
			String docTitle = handleEASEnAXAMapping(TITLE, docName, isOtherDoc);
			String docType = handleEASEnAXAMapping(TYPE, docName, isOtherDoc);
			
			Log.debug("Submit Doc Title after Mapping: " + docTitle);
			Log.debug("Submit Doc Type after Mapping: " + docType);
			
			if (pdfBase64.isEmpty() || pdfBase64 == null || pdfBase64.equalsIgnoreCase("null")) {
				return true;
			} else {
				String strIFullName = "";
				if (quoDoc.has("iFullName")) {
					strIFullName = quoDoc.getString("iFullName");
				} else {
					strIFullName = eappDoc.getJSONObject("quotation").getJSONObject("insureds").getString("iFullName");
				}
				
//				bodyToWfi = prepareBodyForWfi(pdfBase64, eappDoc, quoDoc.getString("iFullName"), eappDoc.getString("policyNumber"), priDocFlag , docTitle, docType);
				bodyToWfi = prepareBodyForWfi(pdfBase64, eappDoc, strIFullName, eappDoc.getString("policyNumber"), priDocFlag , docTitle, docType, isShield, parentAppDoc);
				
				Log.info("Call API ease-api/document");
				
				String response = callpostApi(bodyToWfi, DOMAIN + "/document");
				
				Function f = new Function();
				boolean result = f.handleResponse(response, "RecordDocumentResponse");
				if (result == false) {
					WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
					weException.create(batchNo, policyNum, response);
				}
				
				return result;
			}
		} catch (Exception e) {
			Log.error(e);
			WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
			try {
				weException.create(batchNo, policyNum, convertExceptionToClob(e));
			} catch (Exception e1) {
				Log.error(e1);
			}
			return false;
		}
	}
	
	private boolean submitSysDoc(String docId, JSONObject eappDoc, JSONObject quoDoc, String docName, String policyNum, int batchNo, boolean isShield, boolean primaryDocFlag, String mappingDocName, JSONObject parenteAppDoc){
		try {
			Log.info("Submit Doc Name: " + docName);
			
			JsonObject bodyToWfi;
			
			String base64Str = getAttinBase64(docId, docName);
			
			String priDocFlag = (primaryDocFlag) ? "YES" : "NO";
			
			String docTitle = handleEASEnAXAMapping(TITLE, mappingDocName, false);
			String docType = handleEASEnAXAMapping(TYPE, mappingDocName, false);

			Log.debug("Submit Doc Title after Mapping: " + docTitle);
			Log.debug("Submit Doc Type after Mapping: " + docType);
			
			if (base64Str.isEmpty() || base64Str == null || base64Str.equalsIgnoreCase("null")) {
				return true;
			} else {
				String strIFullName = "";
				if (quoDoc.has("iFullName")) {
					strIFullName = quoDoc.getString("iFullName");
				} else {
					strIFullName = eappDoc.getJSONObject("quotation").getJSONObject("insureds").getString("iFullName");
				}
				
//				bodyToWfi = prepareBodyForWfi(base64Str, eappDoc, quoDoc.getString("iFullName"), eappDoc.getString("policyNumber"), priDocFlag , docTitle, docType);
				bodyToWfi = prepareBodyForWfi(base64Str, eappDoc, strIFullName, eappDoc.getString("policyNumber"), priDocFlag , docTitle, docType, isShield, parenteAppDoc);
				
				Log.info("Call API ease-api/document");
				
				String response = callpostApi(bodyToWfi, DOMAIN + "/document");
				
				Function f = new Function();
				boolean result = f.handleResponse(response, "RecordDocumentResponse"); 
				if (result == false) {
					WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
					weException.create(batchNo, policyNum, response);
				}
				return result;
			}
		} catch (Exception e) {
			Log.error(e);
			WFI_EIP_EXCEPTION weException = new WFI_EIP_EXCEPTION();
			try {
				weException.create(batchNo, policyNum, convertExceptionToClob(e));
			} catch (Exception e1) {
				Log.error(e1);
			}
			return false;
		}
	}
	
	private String getAttinBase64(String docId, String attName) throws Exception {
        String gatewayUser = EnvVariable.get("GATEWAY_USER");
        String gatewayPW = EnvVariable.get("GATEWAY_PW");

		Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
		byte[] returnedByte = Constant.CBUTIL.GetAttach(docId, attName, ".pdf");
		StringBuilder sb = new StringBuilder();
		sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(returnedByte, false)));
		return sb.toString();
	}
	
	private JsonObject prepareBodyForWfi (String attBase64Str, JSONObject eappDoc, String insuredName, String policyId, String flag, String docTitle, String docType, boolean isShield, JSONObject parentAppDoc) throws Exception {
		JsonObject body = new JsonObject();
		String productLine = eappDoc.getJSONObject("quotation").getString("productLine");
		body.addProperty("contentData", attBase64Str);
		body.addProperty("MIMEType", mimeType);
		body.addProperty("creationDate", DATE_FORMAT_ISO8601.format(ZonedDateTime.now(ZoneOffset.UTC)));
		body.addProperty("documentTitle", docTitle);
		body.addProperty("documentType", docType);
		body.addProperty("insuredName", truncateSpecialChar(insuredName));
		body.addProperty("policyNumber", policyId);
		body.addProperty("primaryDocumentFlag", flag);
//		body.addProperty("productType", "UL");
		
		if (productLine.equals("IL")) {
			body.addProperty("productType", "UL");
		} else {
			body.addProperty("productType", "TL");	
		}
		
		if (isShield) {
			String iCid = (eappDoc.has("iCid")) ? eappDoc.getString("iCid") : "";
			String appId = (eappDoc.has("_id")) ? eappDoc.getString("_id") : "";
			body.addProperty("channelCode", "MI");
			body.addProperty("otherPolicyNumber", WfiRlsSubmissionUtil.getPairedPolicyNumberInShield(parentAppDoc, iCid, policyId, appId));
			body.addProperty("basePlanCode", WfiRlsSubmissionUtil.getBasicPlanCodeByCid(parentAppDoc, iCid, appId));			
		} else {
			body.addProperty("channelCode", "AG");
		}

		return body;
	}
	
	private String callgetApi(String path) throws Exception {
		HttpClient httpclient = HttpClients.createDefault();
		
		HttpGet httpget = new HttpGet(path);
		httpget.setHeader("Content-Type", "application/json");

		//Execute and get the response.
		HttpResponse response = httpclient.execute(httpget);
		
		//response.getStatusLine().getStatusCode();

		HttpEntity entity = response.getEntity();
		String responseString = "";
		if (entity != null) {
			responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
		}
		
		return responseString;
	}
	
	private String callpostApi(JsonObject wfiParams, String path) throws Exception{
		HttpClient httpclient = HttpClients.createDefault();
		
		HttpPost httppost = new HttpPost(path);
		httppost.setHeader("Content-Type", "application/json");
		
		httppost.setEntity(new StringEntity(wfiParams.toString(), StandardCharsets.UTF_8.name()));

		//Execute and get the response.
		HttpResponse response = httpclient.execute(httppost);
		
		//response.getStatusLine().getStatusCode();

		HttpEntity entity = response.getEntity();

		if (entity != null) {
			String responseString = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8.name());
			return  responseString;
		} else {
			return "";
		}
	}
	
	private static String convertExceptionToClob(Exception e) {
		StringWriter sw = null;
		PrintWriter pw = null;
		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString();
			return sStackTrace;
		} catch (Exception exp) {
			Log.error(exp);
			return exp.getMessage();
		} finally {
			if(sw != null) {
				try {
					sw.close();
				} catch (IOException e1) {
					Log.error(e1);
				}
			}
			
			if(pw != null) {
				pw.flush();
				pw.close();
			}
		}
		
	}
		
	//Block messages that contain a single-quote ('), hash mark (#), or string (--) anywhere within the messagec
	private String truncateSpecialChar(String data) {
		
		String result = data;
		result = result.replace("&", "&amp;");
		result = result.replace("#", "");
		result = result.replace("--", "");
		result = result.replace("'", "&apos;");
		result = result.replace("\"", "&quot;");
		result = result.replace("<", "&lt;");
		result = result.replace(">", "&gt;");
		
		return result;
	}
}
