package com.eab.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class AgentUtil {
	/**
	 * agentDetails view filter the agent without RawData
	 * @param compCode
	 * @return
	 */
	public static JsonObject getRawDataAgentsByUserId(String compCode) {
		JsonObject result = new JsonObject();
		try {
			Function func = new Function();
			JsonObject viewRows = func.transformObject(Constant.CBUTIL.getRecordsByViewAfterIndex("agentDetails", "[\"" + compCode + "\",\"userId\",\"0\"]", "[\"" + compCode + "\",\"userId\",\"ZZZ\"]"));
			
			if (viewRows == null) {
				return result;
			}
			JsonArray rows = viewRows.getAsJsonArray("rows");

			if (rows.size() == 0){
				return result;
			}
			
			for (int i=0; i< rows.size(); i++){
				try{
					JsonObject transformedObj = rows.get(i).getAsJsonObject().getAsJsonObject("value");
					if (transformedObj != null && transformedObj.get("agentCode") != null && transformedObj.get("rawData").getAsJsonObject().get("userId") != null){
						result.addProperty(transformedObj.get("rawData").getAsJsonObject().get("userId").getAsString(), transformedObj.get("agentCode").getAsString());
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.debug("Missing Object in agentDetails view -- skip");
				}
				
			}
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}		
	}
	
	
	public static JsonObject geAllAgents(String compCode) {
		JsonObject result = new JsonObject();
		try {
			Function func = new Function();
			JSONObject viewRows = Constant.CBUTIL.getRecordsByViewAfterIndex("agentDetails", "[\"" + compCode + "\",\"agentCode\",\"0\"]", "[\"" + compCode + "\",\"agentCode\",\"ZZZ\"]");
			
			if (viewRows == null) {
				return result;
			}
			JSONArray rows = viewRows.getJSONArray("rows");

			if (rows.length() == 0){
				return result;
			}
			JsonObject valueObj;
			
			for (int i=0; i< rows.length(); i++){
				try{
					valueObj = func.transformObject(rows.getJSONObject(i).getJSONObject("value"));
					//JsonElement rawData = transformedObj.get("rawData");
					JsonElement rawData = func.getAsPath(valueObj, "rawData");
					JsonElement proxyEndDate = func.getAsPath(valueObj, "rawData/proxyEndDate");
					JsonElement proxyStartDate = func.getAsPath(valueObj, "rawData/proxyStartDate");
								
					
					if (valueObj != null && rawData != null && proxyEndDate != null && proxyStartDate != null){

						boolean isProxying = compareInProxyPeriod(proxyStartDate.getAsString(), proxyEndDate.getAsString());
						valueObj.addProperty("isProxying", isProxying);
						//valueObj.add("userId", valueObj);
					} else {
						valueObj.addProperty("isProxying", false);
					}
					result.add(valueObj.get("agentCode").getAsString(), valueObj);
				} catch (Exception e) {
					e.printStackTrace();
					Log.debug("Missing Object in agentDetails view -- skip");
				}
				
			}
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}		
	}
	
	public static boolean compareInProxyPeriod(String startDate, String endDate) {
		try {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HHmmssSSS");
			Date sDate = format.parse(startDate + " 000000000");
			ZonedDateTime startTime = ZonedDateTime.ofInstant(sDate.toInstant(), ZoneId.of(Constant.ZONE_ID));
			
			DateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd HHmmssSSS");
			Date eDate = endFormat.parse(endDate + " 235959999");
			
			ZonedDateTime endTime = ZonedDateTime.ofInstant(eDate.toInstant(), ZoneId.of(Constant.ZONE_ID));
			
			ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of(Constant.ZONE_ID));
			
			if(currentTime.isBefore(endTime) && currentTime.isAfter(startTime)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
}


