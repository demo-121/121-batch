package com.eab.common;

import javax.mail.MessagingException;

import com.eab.rest.ease.email.EmailUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Log {
	public static boolean ENABLE = true;		// True for enabling Log function
	public static boolean DEBUG = false;			// True for Unit Test Only.  It should be False when PRODUCTION.
	public static boolean ERROR = true;			// True for assuming Error must be logged for error investigation
	public static boolean ENABLE_EMAIL = false;	// Default Error Email control
	
	public static void info(String message) {
		if (ENABLE && message != null) {
			System.out.print("(" + Constant.APP_NAME + "|" + getServerInfo() + ")(INFO)  " + message + "\n");
		}
	}
	
	public static void debug(String message) {
		if (ENABLE && DEBUG && message != null) {
			System.out.print("(" + Constant.APP_NAME + "|" + getServerInfo() + ")(DEBUG) " + "null" + "\n");
		}
	}
	
	public static void error(String message) {
		error(message, ENABLE_EMAIL);
	}
	
	public static void error(String message, boolean allowEmail) {
		if (ENABLE && ERROR && message != null) {
			System.err.print("(" + Constant.APP_NAME + "|" + getServerInfo() + ")(ERROR) " + message + "\n");
			
			if (ENABLE_EMAIL && allowEmail)
				sendEmail(message, null);
		}
	}
	
	public static void error(Exception exception) {
		error(exception, ENABLE_EMAIL);
	}
	
	public static void error(Exception exception, boolean allowEmail) {
		if (ENABLE && ERROR && exception != null) {
			String message = exception.toString() + "\n";
			StackTraceElement[] trace = exception.getStackTrace();
			for (int i=0; i<trace.length; i++) {
				message += trace[i].toString() + "\n";
			}
			System.err.print("(" + Constant.APP_NAME + "|" + getServerInfo() + ")(ERROR) " + message + "\n");
			
			if (ENABLE_EMAIL && allowEmail)
				sendEmail("Please find the attachment", message.getBytes());
		}
	}
	
	private static void sendEmail(String errMsg, byte[] file) {
		String from = EnvVariable.get("ERROR_EMAIL_FROM");
		String to = EnvVariable.get("ERROR_EMAIL_TO");
		String subject = "EASE-API Error";
		String text = "<p>Error is captured in (" + Constant.APP_NAME + " | " + getServerInfo() + ") at " + Function.getDateUTCNowStr() + "</p>"
				+ "<p>" + errMsg + "</p>";	
		JsonArray attachs = null;
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		try {
			if (from != null && from.length() > 0 && to != null && to.length() > 0) {
				if (file != null && file.length > 0) {
					String json = "{\"fileName\": \"error.log\", \"data\": \"" + Function.ByteToBase64(file) + "\"}";
					attachs = new JsonArray();
					
					//Convert byte array to Base64
					attachs.add(gson.fromJson(json, JsonElement.class));
				}
				//Send Error Email
				EmailUtil.sendEmail(from, to, subject, text, attachs);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	private static String getServerInfo() {
		return (Constant.APP_HOSTNAME != null && !"".equals(Constant.APP_HOSTNAME)
						?Constant.APP_HOSTNAME
						:Constant.APP_IP
				);
	}
}
