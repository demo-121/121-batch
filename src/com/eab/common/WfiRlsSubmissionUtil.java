package com.eab.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class WfiRlsSubmissionUtil {
	public static String getPairedPolicyNumberInShield(JSONObject masterAppDoc, String iCid, String policyNumber, String appId) {
		Function func = new Function();
		JsonObject mAppDoc = func.transformObject(masterAppDoc);
		JsonArray iCidMapping = func.transformEleToArray(func.getAsPath(mAppDoc.getAsJsonObject(), "iCidMapping/" + iCid));
		String pairedPolicyNumber = "";
		for (JsonElement ele : iCidMapping) {
			JsonObject iCidObject = ele.getAsJsonObject();
			String tempPolicyNumber = (iCidObject.has("policyNumber")) ? iCidObject.get("policyNumber").getAsString() : "";
			if (!policyNumber.equals(tempPolicyNumber) && isShieldRiderPlan(appId, masterAppDoc, iCid)) {
				pairedPolicyNumber = tempPolicyNumber;
			}
		}
		
		return pairedPolicyNumber;
	}
	
	public static String getBasicPlanCodeByCid(JSONObject masterAppDoc, String iCid, String appId) {
		Function func = new Function();
		JsonObject mAppDoc = func.transformObject(masterAppDoc);
		JsonObject cidQuotation = func.transformEleToObj(func.getAsPath(mAppDoc.getAsJsonObject(), "quotation/insureds/" + iCid));
		JsonArray quotationPlans = (cidQuotation.has("plans")) ? cidQuotation.get("plans").getAsJsonArray() : null;
		boolean isShieldRiderPlan = isShieldRiderPlan(appId, masterAppDoc, iCid);
		if (quotationPlans != null && quotationPlans.size() > 0 && quotationPlans.get(0) != null && !isShieldRiderPlan) {
			JsonObject basicPlanObect = quotationPlans.get(0).getAsJsonObject();
			return (basicPlanObect.has("planCode")) ? basicPlanObect.get("planCode").getAsString() : "";
		} else if (quotationPlans != null && quotationPlans.size() > 1 && quotationPlans.get(1) != null && isShieldRiderPlan) {
			JsonObject basicPlanObect = quotationPlans.get(1).getAsJsonObject();
			return (basicPlanObect.has("planCode")) ? basicPlanObect.get("planCode").getAsString() : "";
		} else {
			return "";
		}
	}
	
	public static boolean isShieldRiderPlan(String appId, JSONObject masterAppDoc, String iCid) {
		Function func = new Function();
		JsonObject mAppDoc = func.transformObject(masterAppDoc);
		JsonArray iCidMapping = func.transformEleToArray(func.getAsPath(mAppDoc.getAsJsonObject(), "iCidMapping/" + iCid));
		boolean isShieldRiderPlan = false;
		for (JsonElement ele : iCidMapping) {
			JsonObject cidObject = func.transformEleToObj(ele);
			if (cidObject.has("applicationId") 
					&& cidObject.has("covCode") 
					&& cidObject.get("applicationId").getAsString().equals(appId)
					&& cidObject.get("covCode").getAsString().equals("ASP")) {
				isShieldRiderPlan = true;
			}
		}
		
		return isShieldRiderPlan;
	}
	
	public static boolean getAllDependentStatus (String appId, JSONObject eappDoc, JSONObject quoDoc, long minutesDiffFromCreatedDate) throws Exception {
		boolean result = true;
		// Check when this is shield case otherwise return true
		if (eappDoc.has("parentId") && quoDoc.has("quotType") && quoDoc.getString("quotType").equalsIgnoreCase("SHIELD")) {
			
			JSONObject sApplicationDoc = Constant.CBUTIL.getDoc(eappDoc.getString("parentId"));
			
			String currentAppICid = eappDoc.has("iCid") ? eappDoc.getString("iCid") : "";
			
			boolean isShieldRiderPlan = isShieldRiderPlan(appId, sApplicationDoc, currentAppICid);
			
			boolean isTimeReady = minutesDiffFromCreatedDate > Long.parseLong(EnvVariable.get("WEB_API_RLS_SHIELD_RIDER_TIME_LAG"));
			
			//If it is shield rider plan, it will send to wfi and rls after 5 minutes (depend on environment variable)
			result = (isShieldRiderPlan && isTimeReady) || (!isShieldRiderPlan);
		}
		
		return result;
	}
	
}


