package com.eab.dao;

import java.io.ByteArrayInputStream;
import java.util.Date;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DataType;

public class ATTACH_LOG extends BaseDao{
	public boolean create(int seqNo, String batchJobName, String attachName, byte[] attachFile, Date curTime)
			throws Exception {
		
		init();
		Log.info("Log Time");

		String sqlStatement = "insert into ATTACH_LOG (SEQ_NO, BATCH_JOB_NAME, ATTACH_NAME, ATTACH_FILE, CREATE_DATE, CREATE_BY, UPD_DATE, UPD_BY) "
				+  "values (" + seqNo + ", "
				+ dbm.param(batchJobName, DataType.TEXT) + ", "
				+ dbm.param(attachName, DataType.TEXT) + ", "  
				+ dbm.param(attachFile, DataType.BLOB) + ", "  
				+ dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE) + " , 'ATTACH', " 
				+ dbm.param(Function.convertJavaDateToSqlDate(curTime), DataType.DATE) + " , 'ATTACH')";
		
		Log.info(sqlStatement);
		return insert(sqlStatement);
	}
 
	
	public int selectSeq() throws Exception{
		init();
		
		String sqlStatement = "select attach_log_no_seq.nextval from dual";
		
		return selectSingleRecord(sqlStatement).getInt("NEXTVAL");
	}

}
