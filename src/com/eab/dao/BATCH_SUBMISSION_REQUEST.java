package com.eab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.ZonedDateTime;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;

public class BATCH_SUBMISSION_REQUEST extends BaseDao{

	public boolean create(String appNo, String statusToTrigger, int batchNo, String submissionStatus)
			throws Exception {
		
		init();
		
		ZonedDateTime currentDate = Function.getDateUTCNow();
		
		String sqlStatement = "insert into BATCH_SUBMISSION_REQUEST(EAPP_NO, CREATE_DATE, TRIGGER_BY_PROCESS, CREATE_BY_BATCH_NO, SUBMISSION_DATE_ST, SUBMISSION_STATUS) "
				+  "values (" + dbm.param(appNo, DataType.TEXT) + ", "
				+ dbm.param(Function.covertZonedDateTimeToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(statusToTrigger, DataType.TEXT) + ", "
				+ dbm.param(batchNo, DataType.INT) + ", "
				+ dbm.param(Function.covertZonedDateTimeToSqlDate(currentDate), DataType.DATE) + ", "
				+ dbm.param(submissionStatus, DataType.TEXT) + ")";

		return insert(sqlStatement);
	}
	
	public boolean hasPrimaryKey(String key) throws Exception{
		Connection conn = null;		
		
		PreparedStatement stmt = null;
		
		String result = null;
		 
		try {
			conn = DBAccess.getConnection();	
			String sql = " select EAPP_NO"
					   + " from BATCH_SUBMISSION_REQUEST"
					   + " where EAPP_NO = ?";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, key);
			
			ResultSet rs = stmt.executeQuery();

			if(rs != null && rs.next()){
				return true;
			} else {
				return false;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {				 
				Log.error(e2);
			}
		}
	}


	public boolean update(String eappId, int noOfRec, String batchStatus) throws Exception {
		
		init();
		
		ZonedDateTime currentDate = Function.getDateUTCNow();
		
		String sqlStatement = "UPDATE BATCH_SUBMISSION_REQUEST SET SUBMISSION_DATE_END = " + dbm.param(Function.covertZonedDateTimeToSqlDate(currentDate), DataType.DATE)
				+ " , NUM_OF_DOCS = " + dbm.param(noOfRec, DataType.INT)
				+ " , SUBMISSION_STATUS = " + dbm.param(batchStatus, DataType.TEXT)
				+ " WHERE EAPP_NO = "
				+ dbm.param(eappId, DataType.TEXT);

		return update(sqlStatement);
	}
	
	
	public boolean updateRecordtoTake(int bNo, int noOfRecordToTake) throws Exception {
		init();
		
		String sqlStatement = "UPDATE batch_submission_request set taken_by_batch_no = " +  dbm.param(bNo, DataType.INT) + ", SUBMISSION_STATUS = 'P' where EAPP_NO in "
				+ "(SELECT * FROM (SELECT EAPP_NO FROM batch_submission_request where taken_by_batch_no is null ORDER BY create_date ) WHERE ROWNUM <= " + dbm.param(noOfRecordToTake, DataType.INT) + ")";
		Log.debug(sqlStatement);
		return update(sqlStatement);
	}
	
	public int selectSeq() throws Exception{
		init();
		
		String sqlStatement = "select batch_sub_request_seq.nextval from dual";
		
		return selectSingleRecord(sqlStatement).getInt("NEXTVAL");
	}
	
	public JSONObject singlerecordquerySQL(String Sql) throws Exception {
		init();
		
		return selectSingleRecord(Sql); 
	}
	
	public JSONArray querySQL(String Sql) throws Exception {
		init();
		
		return selectMultipleRecords(Sql); 
	}
	
	public JSONArray selectSubmittedCasesToRLS(java.sql.Date fromTime, java.sql.Date toTime) throws Exception {
		init();
		
		String sqlStatement = "SELECT EAPP_NO, to_char( cast(SUBMISSION_DATE_END as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as SUBMISSION_DATE_END FROM BATCH_SUBMISSION_REQUEST WHERE SUBMISSION_STATUS = 'C' AND (SUBMISSION_DATE_END BETWEEN "+ dbm.param(fromTime, DataType.DATE)+" AND "+dbm.param(toTime, DataType.DATE)+") ORDER BY SUBMISSION_DATE_END ASC";
		
		return selectMultipleRecords(sqlStatement); 
	}
	
	public boolean rollbackForResubmission(String appId) throws Exception {
		init();
		
		String sqlStatement = "UPDATE BATCH_SUBMISSION_REQUEST set Taken_By_Batch_No = null, SUBMISSION_STATUS = 'R' where EAPP_NO = " + dbm.param(appId, DataType.TEXT);

		return update(sqlStatement);
	}
}
