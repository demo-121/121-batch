package com.eab.dao;

import java.sql.Connection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;

public class BaseDao {

	DBManager dbm = null;
	Connection conn = null;

	protected void init() {
		dbm = new DBManager();
	}

	protected boolean insert(String sqlStatement) throws Exception {

		boolean result = false;

		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);

			result = dbm.insert(sqlStatement, conn);

			if (result) {
				conn.commit();
			} else {
				conn.rollback();
			}

		} catch (Exception ex) {
			Log.error(ex);
			throw ex;
		} finally {
			deallocate();
		}

		return result;

	}

	protected boolean insertBatch(String sqlStatement, List<Object> sqlStatementDataObjList, List<Integer> dataTypeList)
			throws Exception {

		boolean result = false;

		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);

			result = dbm.insertBatch(sqlStatement, sqlStatementDataObjList, dataTypeList, conn);

			if (result) {
				conn.commit();
			} else {
				conn.rollback();
			}

		} catch (Exception ex) {
			Log.error(ex);
			throw ex;
		} finally {
			deallocate();
		}

		return result;

	}

	protected JSONObject selectSingleRecord(String sqlStatement) throws Exception {

		JSONObject resultJson = null;

		try {
			conn = DBAccess.getConnection();

			resultJson = dbm.selectRecord(sqlStatement, conn);

		} catch (Exception ex) {
			Log.error(ex);
			throw ex;
		} finally {
			deallocate();
		}

		return resultJson;

	}

	protected JSONArray selectMultipleRecords(String sqlStatement) throws Exception {

		JSONArray resultArrayJson = null;

		try {
			conn = DBAccess.getConnection();

			resultArrayJson = dbm.selectRecords(sqlStatement, conn);

		} catch (Exception ex) {
			Log.error(ex);
			throw ex;
		} finally {
			deallocate();
		}

		return resultArrayJson;

	}

	protected boolean update(String sqlStatement) throws Exception {

		boolean result = false;

		try {
			conn = DBAccess.getConnection();

			int count = dbm.update(sqlStatement, conn);

			if (count > 0)
				result = true;
		} catch (Exception e) {
			throw e;
		} finally {
			deallocate();
		}

		return result;
	}

	protected boolean delete(String sqlStatement) throws Exception {

		boolean result = false;

		try {
			conn = DBAccess.getConnection();

			int count = dbm.delete(sqlStatement, conn);

			if (count == 1)
				result = true;
		} catch (Exception e) {
			throw e;
		} finally {
			deallocate();
		}

		return result;
	}
	
	protected boolean deleteBatch(String sqlStatement, List<Object> sqlStatementDataObjList, List<Integer> dataTypeList)
			throws Exception {

		boolean result = false;

		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);

			result = dbm.deleteBatch(sqlStatement, sqlStatementDataObjList, dataTypeList, conn);

			if (result) {
				conn.commit();
			} else {
				conn.rollback();
			}

		} catch (Exception ex) {
			Log.error(ex);
			throw ex;
		} finally {
			deallocate();
		}

		return result;

	}

	private void deallocate() throws Exception {
		dbm.clear();
		dbm = null;
		if (conn != null && !conn.isClosed())
			conn.close();
		conn = null;
	}

}
