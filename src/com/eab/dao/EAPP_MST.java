package com.eab.dao;

import java.util.Date;

import com.eab.database.jdbc.DataType;

public class EAPP_MST extends BaseDao{

	public boolean create(String eappNo, String agentCode, String polNumber, String bundleId, String appCreateISOString, String signatureISOString, String eappStatus, String paymentMethod, String User)
			throws Exception {
		
		init();

		String sqlStatement = "insert into EAPP_MST(EAPP_NO, AGENT_CODE, POL_NUM, BUNDLE_ID, APP_CREATE_ISOSTRING,  SIGNATURE_ISOSTRING, EAPP_STATUS, PAYMENT_MTD, CREATE_DATE, CREATE_BY, UPD_DATE, UPD_BY)" + " values("
				+ dbm.param(eappNo, DataType.TEXT) + ", " + dbm.param(agentCode, DataType.TEXT) + ", "
				+ dbm.param(polNumber, DataType.TEXT) + ", " + dbm.param(bundleId, DataType.TEXT) + ", "
				+ dbm.param(appCreateISOString, DataType.TEXT) + ", " + dbm.param(signatureISOString, DataType.TEXT) + ", "
				+ dbm.param(eappStatus, DataType.TEXT) + ", " + dbm.param(paymentMethod, DataType.TEXT) + ", "
				+ "sysdate, " + dbm.param(User, DataType.TEXT) + ", sysdate, " + dbm.param(User, DataType.TEXT) + ")";

		return insert(sqlStatement);
	}
}
