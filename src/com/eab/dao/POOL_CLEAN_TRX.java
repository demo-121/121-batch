package com.eab.dao;

import com.eab.database.jdbc.DataType;

public class POOL_CLEAN_TRX extends BaseDao {
	
	public boolean create(String uuid) throws Exception {

		init();

		String sqlStatement = "insert into POOL_CLEAN_TRX(UUID)" + " values("
				+ dbm.param(uuid, DataType.TEXT)
				+ ")";

		return insert(sqlStatement);
	}
	
	public boolean update(String uuid, int countUsedShieldPool, int countUsedNonShieldPool)
			throws Exception {

		init();
		
		String sqlStatement = "UPDATE POOL_CLEAN_TRX SET SHLD_USED_NUM = "
				+ dbm.param(countUsedShieldPool, DataType.INT) 
				+ " , NON_SHLD_USED_NUM = " + dbm.param(countUsedNonShieldPool, DataType.INT) 
				+ " WHERE UUID = "+ dbm.param(uuid, DataType.TEXT);

		return update(sqlStatement);
	}

}
