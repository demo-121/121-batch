package com.eab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;

public class SYSTEM_PARAM extends BaseDao{
	
	public String selectSysValue(String key) throws Exception{
		Connection conn = null;		
		
		PreparedStatement stmt = null;
		
		String result = null;
		 
		try {
			conn = DBAccess.getConnection();	
			String sql = " select SYSPARAM_VALUE"
					   + " from SYSTEM_PARAM"
					   + " where SYSPARAM_ID = ?";

			stmt = conn.prepareStatement(sql);
			stmt.setString(1, key);
			
			ResultSet rs = stmt.executeQuery();

			if(rs != null){
				while(rs.next()) {
					result = rs.getObject("SYSPARAM_VALUE").toString();					
				}
				return result;
			} else {
				return null;
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {				 
				Log.error(e2);
			}
		}
	}
}
