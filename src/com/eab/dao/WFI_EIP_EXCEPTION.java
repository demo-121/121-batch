package com.eab.dao;

import com.eab.common.Log;
import com.eab.database.jdbc.DataType;

public class WFI_EIP_EXCEPTION extends BaseDao{
	public boolean create(int batchNo, String policyNumber, String exceptionDesc)
			throws Exception {
		
		init();
		if (exceptionDesc != null) {
			int length = (exceptionDesc.length() > 3999) ? 3999 : exceptionDesc.length();
			exceptionDesc = exceptionDesc.substring(0, length);
		}
		
		String sqlStatement = "insert into WFI_EIP_EXCEPTION (POL_NUM, EXCP_DESC, CREATE_DATE, CREATE_BY_BATCH_NO) "
				+  "values (" + dbm.param(policyNumber, DataType.TEXT) + ", "
				+  dbm.param(exceptionDesc, DataType.CLOB) + ", sysdate,"
				+ dbm.param(batchNo, DataType.INT) + ")";
		
		return insert(sqlStatement);
	}
}
