package com.eab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.ws.rs.core.Response;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;
import com.eab.object.WFI_EASE_MAPPING;

public class WFI_TITLE_TYPE_MAPPING extends BaseDao{	
	public ArrayList<WFI_EASE_MAPPING> enquiryMapping() throws Exception {
		ArrayList<WFI_EASE_MAPPING> wfiMapping = new ArrayList<WFI_EASE_MAPPING>();
		Connection conn = null;		
		
		PreparedStatement stmt = null;
		 
		try {
			conn = DBAccess.getConnection();	
			String sql = " select EASE_ID, AXA_SG_TITLE, AXA_SG_TYPE"
					   + " from WFI_TITLE_TYPE_MAPPING";

			stmt = conn.prepareStatement(sql);
			
			ResultSet rs = stmt.executeQuery();

			if(rs != null){
				while(rs.next()) {
					wfiMapping.add(new WFI_EASE_MAPPING(
							rs.getObject("EASE_ID").toString(), rs.getObject("AXA_SG_TITLE").toString(), rs.getObject("AXA_SG_TYPE").toString()));					
				}
				return wfiMapping;
			} else {
				return null;
			}
		}
		catch (Exception ex) {
			Log.error(ex);
			throw ex;
		}
		finally {
			try {
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e2) {				 
				Log.error(e2);
			}
		}
	}
}
