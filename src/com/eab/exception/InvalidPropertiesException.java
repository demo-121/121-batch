package com.eab.exception;

@SuppressWarnings("serial")
public class InvalidPropertiesException extends Exception {
	public InvalidPropertiesException(String parameter) {
		super(String.format("Required paramter %s is missing", parameter));
	}
}
