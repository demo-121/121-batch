package com.eab.object;

public class AgentProfile {
	private String managerCode;
	private String agentCode;
	private String name;
	private String mobile;
	private String email;
	private boolean isProxying;
	
	public AgentProfile() {
	}
	
	public AgentProfile(String agentCode, String managerCode, String name, String mobile, String email, boolean isProxying) {
		super();
		this.agentCode = agentCode;
		this.managerCode = managerCode;
		this.name = name;
		this.mobile = mobile;
		this.email = email;
		this.isProxying = isProxying;
	}

	public String getAgentCode(){
		return agentCode;
	}
	
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getManagerCode() {
		return managerCode;
	}

	public void setManagerCode(String managerCode) {
		this.managerCode = managerCode;
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean getIsProxying() {
		return isProxying;
	}
	
	public void setIsProxying(boolean isProxying) {
		this.isProxying = isProxying;
	}

	@Override
	public String toString() {
		return "Email [Manager Code=" + managerCode + ", Agent Code=" + agentCode + ", Name=" + name + ", Mobile=" + mobile + ", Email=" + email + ", Is Proxying=" + String.valueOf(isProxying)
				+ "]";
	}
}
