package com.eab.object;

public class Email {
	private String sender;
	private String recipients;
	private String title;
	private String content;
	private String cc;
	
	public Email() {
	}
	
	public Email(String sender, String recipients, String cc, String title, String content) {
		super();
		this.sender = sender;
		this.recipients = recipients;
		this.cc = cc;
		this.title = title;
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public String getCc(){
		return cc;
	}
	
	public void setCc(String cc) {
		this.cc = cc;
	}
	
	public String getRecipients() {
		return recipients;
	}
	
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Email [sender=" + sender + ", recipients=" + recipients + ", title=" + title + ", content=" + content
				+ "]";
	}
}
