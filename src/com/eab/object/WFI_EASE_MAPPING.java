package com.eab.object;

public class WFI_EASE_MAPPING {
	private String EASE_ID;
	private String AXA_SG_TITLE;
	private String AXA_SG_TYPE;
	
	public WFI_EASE_MAPPING() {
	}
	
	public WFI_EASE_MAPPING(String easeId, String sgTitle, String sgType) {
		super();
		this.EASE_ID = easeId;
		this.AXA_SG_TITLE = sgTitle;
		this.AXA_SG_TYPE = sgType;
	}

	public String getEaseId() {
		return EASE_ID;
	}

	public void setEaseId(String id) {
		this.EASE_ID = id;
	}
	
	public String getSgTitle(){
		return AXA_SG_TITLE;
	}
	
	public void setSgTitle(String title) {
		this.AXA_SG_TITLE = title;
	}
	
	public String getSgType() {
		return AXA_SG_TYPE;
	}
	
	public void setSgType(String type) {
		this.AXA_SG_TYPE = type;
	}

	@Override
	public String toString(){
	    return "(" + EASE_ID + "," + AXA_SG_TITLE + "," + AXA_SG_TYPE  + ",)";
	}
}
