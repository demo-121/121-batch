package com.eab.quartz.listener;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.eab.common.Log;

public class SchedulerGlobalListener implements JobListener {
	private String name;

	public SchedulerGlobalListener() {
		this.name = "EAB-121-Listener";
	}

//	public SchedulerGlobalListener(String name) {
//		if (name.isEmpty()) {
//			this.name = "EAB-GlobalListener";
//		} else {
//			this.name = name;
//		}
//	}

	@Override
	public String getName() {
		return name;
	}

	public String setName(String name) {
		return name;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		Log.info(">>> jobToBeExecuted >>> - Job Name: " + context.getJobDetail().getKey().toString());
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		Log.debug(">>> jobWasExecuted >>> - Job Name: " + context.getJobDetail().getKey().toString() + " | Job Class: " + context.getJobDetail().getJobClass().getName());
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		Log.info(">>> jobExecutionVetoed >>> - Job Name: " + context.getJobDetail().getKey().toString());
	}
}
