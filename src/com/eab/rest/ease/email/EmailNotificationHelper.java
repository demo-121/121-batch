package com.eab.rest.ease.email;

import javax.ws.rs.core.Response;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import com.eab.common.EnvVariable;
import com.eab.common.HttpUtil;
import com.eab.common.Log;
import com.eab.dao.BATCH_EMAIL_TEMPLATE;
import com.eab.dao.SYSTEM_PARAM;
import com.google.gson.JsonObject;

public class EmailNotificationHelper {

	private final static String NOTI_ERROR_EMAIL_WFI = "NOTI_ERROR_EMAIL_WFI";
	private final static String NOTI_ERROR_EMAIL_PAYMENT = "NOTI_ERROR_EMAIL_PAYMENT";
	private final static String NOTI_ERROR_EMAIL_RLS = "NOTI_ERROR_EMAIL_RLS";
	private final static String NOTI_ERROR_EMAIL_POL = "NOTI_ERROR_EMAIL_POL";

	private final static String NOTI_ERROR_EMAIL_TO = "NOTI_ERROR_EMAIL_TO";
	private final static String NOTI_ERROR_EMAIL_FROM = "NOTI_ERROR_EMAIL_FROM";

	private final static String DOMAIN = EnvVariable.get("INTERNAL_API_DOMAIN");
	private final static String SERVICE_PATH = "/email/sendEmail";

	public static void sendErrorNotificationWFI(String policyNumber) {

		JsonObject jsonBody = createExceptionJsonBody(NOTI_ERROR_EMAIL_FROM, NOTI_ERROR_EMAIL_TO, NOTI_ERROR_EMAIL_WFI,
				policyNumber);

		if (jsonBody != null) {
			sendEmail(jsonBody);
		}
	}

	public static void sendErrorNotificationPayment(String policyNumber) {

		JsonObject jsonBody = createExceptionJsonBody(NOTI_ERROR_EMAIL_FROM, NOTI_ERROR_EMAIL_TO,
				NOTI_ERROR_EMAIL_PAYMENT, policyNumber);

		if (jsonBody != null) {
			sendEmail(jsonBody);
		}
	}

	public static void sendErrorNotificationRLS(String policyNumber) {

		JsonObject jsonBody = createExceptionJsonBody(NOTI_ERROR_EMAIL_FROM, NOTI_ERROR_EMAIL_TO, NOTI_ERROR_EMAIL_RLS,
				policyNumber);

		if (jsonBody != null) {
			sendEmail(jsonBody);
		}
	}

	public static void sendErrorNotificationPolicyNumber() {

		JsonObject jsonBody = createExceptionJsonBody(NOTI_ERROR_EMAIL_FROM, NOTI_ERROR_EMAIL_TO, NOTI_ERROR_EMAIL_POL,
				null);

		if (jsonBody != null) {
			sendEmail(jsonBody);
		}
	}

	private static void sendEmail(JsonObject jsonBody) {
		try {
			if (jsonBody != null) {
				HttpPost postRequest = createEmailPostRequest(jsonBody);
				Response output = HttpUtil.send(postRequest);

				boolean isCompleted = output.getStatus() == 204 ? true : false;
				Log.debug("**** Send Email=" + isCompleted);

			} else {
				Log.error("ERROR - EmailNotificationHelper.sendErrorNotificationWFI jsonBody is null");
			}
		} catch (Exception ex) {
			Log.error(ex);
		}
	}

	private static HttpPost createEmailPostRequest(JsonObject jsonBody) throws Exception {
		Log.info("Goto EmailNotificationHelper.createEmailPostRequest");

		String emailURL = DOMAIN + SERVICE_PATH;

		HttpPost request = new HttpPost(emailURL);
		request.addHeader("Content-Type", "application/json");

		// StringEntity
		StringEntity stringEntity = new StringEntity(jsonBody.toString());
		request.setEntity(stringEntity);

		return request;
	}

	private static JsonObject createExceptionJsonBody(String fromEmailKey, String toEmailKey, String emailTemplateKey,
			String policyNumber) {
		Log.info("Goto EmailNotificationHelper.createExceptionJsonBody");

		Log.debug(
				"*** fromEmailKey=" + fromEmailKey + ", toEmailKey=" + toEmailKey + ", toEmailKey=" + emailTemplateKey);

		JsonObject jsonBody = null;

		try {
			SYSTEM_PARAM eSysParam = new SYSTEM_PARAM();
			String toEmail = eSysParam.selectSysValue(toEmailKey);
			String fromEmail = eSysParam.selectSysValue(fromEmailKey);

			Log.debug("*** toEmail=" + toEmail + ", fromEmail=" + fromEmail);

			BATCH_EMAIL_TEMPLATE emailTemplateJson = new BATCH_EMAIL_TEMPLATE();

			JSONObject templateJsonObject = emailTemplateJson.selectEmailTemplate(emailTemplateKey);

			Log.debug("*** templateJsonObject=" + templateJsonObject.toString());

			String title = templateJsonObject.has("MAIL_SUBJ") ? templateJsonObject.getString("MAIL_SUBJ") : null;
			String content = templateJsonObject.has("MAIL_CONTENT") ? templateJsonObject.getString("MAIL_CONTENT")
					: null;
			
			if(policyNumber!=null) {
				title = title.replace("[pol_no]", policyNumber);// policyNumber
				content = content.replace("[pol_no]", policyNumber);// policyNumber
			}

			if (toEmail != null && fromEmail != null && title != null && content != null) {

				jsonBody = new JsonObject();
				jsonBody.addProperty("to", toEmail);
				jsonBody.addProperty("from", fromEmail);
				jsonBody.addProperty("title", title);
				jsonBody.addProperty("content", content);
			} else {
				Log.error("ERROR - Password - toEmail, fromEmail, title, content have null");
			}

		} catch (Exception ex) {
			Log.error(ex);
		}
		return jsonBody;
	}

}
