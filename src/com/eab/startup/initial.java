package com.eab.startup;





import java.io.File;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;

import com.eab.common.Constant;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.couchbase.CBUtil;
import com.eab.database.jdbc.DBAccess;

public class initial extends HttpServlet {

	public void destroy() {
		//Nothing
	}
	
	public void init(ServletConfig config) throws ServletException {
		Log.info("Initialzation ......Start");
		
		super.init(config);
		try {
			// Get App Server IP Address & Hostname
			Constant.APP_IP = Function.getServerIp();
			Constant.APP_HOSTNAME = Function.getServerName();
			
			// Load Oracle Datasource
			Context context = new InitialContext(); 
			Constant.DATABASE_TYPE = config.getInitParameter("dbtype");
			Log.info("Database Type: " + Constant.DATABASE_TYPE);
			Constant.DATASOURCE_STRING = config.getInitParameter("datasource");
			Log.info("DataSource: " + Constant.DATASOURCE_STRING);
			
			try {
				DBAccess.setDatasource((DataSource) context.lookup(Constant.DATASOURCE_STRING));
			} catch(Exception e) {
				Log.error("[Initial Error] Unable to connect database " + Constant.DATABASE_TYPE + " | " + Constant.DATASOURCE_STRING);
			}
			
			// Load Coucbhase Sync Gateway Connection
	        String gatewayUser = EnvVariable.get("GATEWAY_USER");
	        String gatewayPW = EnvVariable.get("GATEWAY_PW");

			Constant.CBUTIL = new CBUtil(EnvVariable.get("GATEWAY_URL"), EnvVariable.get("GATEWAY_PORT"), EnvVariable.get("GATEWAY_DBNAME"), gatewayUser, gatewayPW);
			
			// Hibernate SessionFactory
//			Constant.SESSION_FACTORY = (SessionFactory) getServletContext().getAttribute("SessionFactory");
			
			// Initial MAAM Token
//			MaamUtil.updateToken(this.getClass().getName());
			String sep = File.separator;
			Constant.TEMPLATE_PATH = this.getServletContext().getRealPath("." + sep) + sep + "WEB-INF" + sep + "classes" + sep + "template" + sep;
			
			Log.debug("Using getServletContext to get the real path");
			Log.debug(Constant.TEMPLATE_PATH);
			
		}  catch(Exception e) {
			Log.error(e);
		} finally {
			///TODO
		}
		
		Log.info("Initialzation ......End");
	}
}
